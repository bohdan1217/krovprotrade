<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("�������");
?>

<section id="basket">
	<div class="container-fluide">
		<div class="row fix">
			<h2>�������</h2>

<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "new", Array(
	"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",	// ������������ ������ ��� ������ ������� (�� ��� ���������� ������)
		"COLUMNS_LIST" => array(	// ��������� �������
			0 => "NAME",
			1 => "DISCOUNT",
			2 => "PROPS",
			3 => "DELETE",
		),
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"PATH_TO_ORDER" => "/personal/order/make/",	// �������� ���������� ������
		"HIDE_COUPON" => "N",	// �������� ���� ����� ������
		"QUANTITY_FLOAT" => "N",	// ������������ ������� �������� ����������
		"PRICE_VAT_SHOW_VALUE" => "Y",	// ���������� �������� ���
		"TEMPLATE_THEME" => "site",	// �������� ����
		"SET_TITLE" => "Y",	// ������������� ��������� ��������
		"AJAX_OPTION_ADDITIONAL" => "",
		"OFFERS_PROPS" => "",	// ��������, �������� �� �������� �������
		"COMPONENT_TEMPLATE" => "personal_cart",
		"USE_PREPAYMENT" => "N",	// ������������ ��������������� ��� ���������� ������ (PayPal Express Checkout)
		"AUTO_CALCULATION" => "Y",	// ������������ �������
		"ACTION_VARIABLE" => "basketAction",	// �������� ���������� ��������
		"USE_GIFTS" => "Y",	// ���������� ���� "�������"
		"GIFTS_PLACE" => "TOP",	// ����� ����� "�������"
		"GIFTS_BLOCK_TITLE" => "�������� ���� �� ��������",	// ����� ��������� "�������"
		"GIFTS_HIDE_BLOCK_TITLE" => "N",	// ������ ��������� "�������"
		"GIFTS_TEXT_LABEL_GIFT" => "�������",	// ����� ����� "�������"
		"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "",	// �������� ����������, � ������� ���������� ���������� ������
		"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",	// �������� ����������, � ������� ���������� �������������� ������
		"GIFTS_SHOW_OLD_PRICE" => "N",	// ���������� ������ ����
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",	// ���������� ������� ������
		"GIFTS_SHOW_NAME" => "Y",	// ���������� ��������
		"GIFTS_SHOW_IMAGE" => "Y",	// ���������� �����������
		"GIFTS_MESS_BTN_BUY" => "�������",	// ����� ������ "�������"
		"GIFTS_MESS_BTN_DETAIL" => "���������",	// ����� ������ "���������"
		"GIFTS_PAGE_ELEMENT_COUNT" => "4",	// ���������� ��������� � ������
		"GIFTS_CONVERT_CURRENCY" => "N",	// ���������� ���� � ����� ������
		"GIFTS_HIDE_NOT_AVAILABLE" => "N",	// �� ���������� ������, ������� ��� �� �������
	),
	false
);?>

		</div>
	</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>