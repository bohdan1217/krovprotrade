<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="row fix">
	<a data-fancybox="group" href="<?=$arResult['DETAIL_PICTURE']['SRC']?>" >
		<img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" />
	</a>
	<?foreach ($arResult['PROPERTIES']['IMAGES']['VALUE'] as $image):?>
		<a data-fancybox="group" href="<?=CFile::GetPath($image);?>" >
			<img src="<?=CFile::GetPath($image);?>" />
		</a>
	<?endforeach;?>
</div>