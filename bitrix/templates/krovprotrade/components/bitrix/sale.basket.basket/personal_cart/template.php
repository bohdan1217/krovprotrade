<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixBasketComponent $component */

?>



<?
	if (!empty($arResult["WARNING_MESSAGE"]) && is_array($arResult["WARNING_MESSAGE"]))
	{
		foreach ($arResult["WARNING_MESSAGE"] as $v)
			ShowError($v);
	}
?>

	<table>
		<thead>
			<tr>
				<td>������������</td>
				<td>����</td>
				<td>����������</td>
				<td>�����</td>
			</tr>
		</thead>

		<tbody>
			<?foreach ($arResult['GRID']['ROWS'] as $arItem):?>
				<tr>
					<td><?=$arItem['NAME']?></td>
					<td><?=number_format($arItem['BASE_PRICE'], 0, '.', '')?> ���.</td>
					<td class="form_wrap">
						<div class="count">
							<input type="text" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem['QUANTITY']?>" placeholder="1" />
							<div class="count-btns">
								<button class="plus">+</button>
								<button class="minus">-</button>
							</div>
						</div>
						<button name="BasketRefresh" value="BasketRefresh" class="update"></button>
						<button data-id="<?=$arItem['ID']?>" name="<?=$arParams["ACTION_VARIABLE"]?>" value="delete" class="cancel"></button>
					</td>
					<td><?=number_format($arItem['BASE_PRICE'] * $arItem['QUANTITY'], 0, '.', '')?> ���.</td>
				</tr>
			<?endforeach;?>
		</tbody>
	</table>

	<input type="hidden" name="id" value="" />

	<div class="ordering">
		<span>�����</span>
		<span><?=number_format($arResult['allSum'], 0, '.', '')?> ���.</span>
		
		<button name="BasketOrder" value="BasketOrder" type="submit" class="button-dark order-btn">
			�������� �����
		</button>



	</div>
	
	<?
	if (strlen($arResult["ERROR_MESSAGE"]) > 0) {
		ShowError($arResult["ERROR_MESSAGE"]);
	}
	?>
