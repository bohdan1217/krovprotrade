<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
echo ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;

if ($normalCount > 0):
?>


						<?
												$discount_price = 0;
												$counter = 0;
												$one_click_id = '';


												foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
													$arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
													if ($arHeader["name"] == '')
														$arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
													$arHeaders[] = $arHeader["id"];

													// remember which values should be shown not in the separate columns, but inside other columns
													if (in_array($arHeader["id"], array("TYPE")))
													{
														$bPriceType = true;
														continue;
													}
													elseif ($arHeader["id"] == "PROPS")
													{
														$bPropsColumn = true;
														continue;
													}
													elseif ($arHeader["id"] == "DELAY")
													{
														$bDelayColumn = true;
														continue;
													}
													elseif ($arHeader["id"] == "DELETE")
													{
														$bDeleteColumn = true;
														continue;
													}
													elseif ($arHeader["id"] == "WEIGHT")
													{
														$bWeightColumn = true;
													}

													if ($arHeader["id"] == "NAME"):
													?>
													<?
													elseif ($arHeader["id"] == "PRICE"):
													?>
													<?
													else:
													?>
													<?
													endif;
													?>
												<?
												endforeach;

												if ($bDeleteColumn || $bDelayColumn):
												?>
													<!-- <td class="custom"></td> -->
												<?
												endif;
												?>

												<?php
													$super_total = 0;
													$super_total_all = 0;
												?>

													<?php
															// echo "<pre>";
															// print_r($arResult["GRID"]["ROWS"]);
															// echo "</pre>";
													$product_counter = 0;
													 ?>

													<?php foreach ($arResult["GRID"]["ROWS"] as $key => $arItem): ?>
														<?php
														
															if($arItem['DELAY'] == 'Y' || $arItem['NOT_AVAILABLE'])
																continue;
															$product_counter++; 
															
															//$ar_res = CCatalogProduct::GetByID($arItem["PRODUCT_ID"]); 
														//var_dumP($arItem['ID']);
														$detail_url = $arItem['DETAIL_PAGE_URL'];
														if(!empty($arItem['PROPERTY_CML2_LINK_VALUE'])){
															CModule::IncludeModule("iblock");

															$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
															$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK, "ACTIVE"=>"Y", 'ID' => $arItem['PROPERTY_CML2_LINK_VALUE']);
															$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
															$total = $res -> SelectedRowsCount();
															while($ob = $res->GetNextElement()){
																$arFields = $ob->GetFields();
																$detail_url = $arFields['DETAIL_PAGE_URL'];
															}
														}
														?>

														<div class="modal__busket__item" id="basket_item_<?=$arItem['ID']?>" >

															<div class="row border_top">
													                <div class="col-sm-3 col-md-3 col-sm-offset-1 col-md-offset-1 marg_left">
													                <?php if($arItem['PREVIEW_PICTURE']){ ?>
									                            		<?php $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], 
																			array('width'=>140, 'height' => 108), BX_RESIZE_IMAGE_PROPORTIONAL, true);  ?>
																			<a href="<?=$detail_url?>" class="img_cart_heard">
									                            				<img src="<?=$file['src']?>" alt="<?=$arItem['NAME']?>" style="height: 108px; width: 140px">
									                            			</a>
									                            	<?php }
									                            		else {
									                            			?>
									                            			<a href="<?=$detail_url?>" class="img_cart_heard">
						                  										<img src="<?=SITE_TEMPLATE_PATH?>/img/default.jpg" alt="<?=$arItem["NAME"]?>" style="height: 108px; width: 140px">
					                  										</a>
									                            			<?
									                            		}
									                            	?>
													                </div>

													                <?php
																		$show_price = CCatalogProduct::GetOptimalPrice($arItem["PRODUCT_ID"], 1, 'N');
																		//var_dump($show_price);
																		$show_price[0] = $show_price['RESULT_PRICE']["DISCOUNT_PRICE"];
																		$show_price[1] = $show_price['RESULT_PRICE']["BASE_PRICE"];
												                  	

												                  		$total = $show_price[0] * $arItem["QUANTITY"];
												                  		$total_all = $show_price[1] * $arItem["QUANTITY"];

														                $super_total += $total;
														                $super_total_all += $total_all;
												                  	?>

													                <div class="col-sm-8 col-md-8">
														                <h4><?=$arItem['NAME']?></h4>
														                <p>Кол-во : <?=$arItem["QUANTITY"]?></p>
														                <p>Цена: <span
													                  		class='js_price total_span total_price_<?=$arItem['ID']?>'
													                  		id="total_price_<?=$arItem['ID']?>"
												                			data-full_price="<?=$arItem['PRICE']?>"
												                			data-discount_percent="<?=$arItem['DISCOUNT_PRICE_PERCENT']?>" 
												                			data-total_all="<?=$super_total_all?>"
												                			data-total="<?=$total?>"
													                  	><?=$total?></span>р.</p>
													                </div>
													        </div>
													    </div>
												        <div class="ns-item-group-wr"></div>
					       							 	<? $discount_price += (float) $arItem['DISCOUNT_PRICE'] * (int) $arItem["QUANTITY"]; ?>
														<?php endforeach; ?>
															<div class="ns-item-group-price">
													         	<div class="item-group ns-item-group">
											                      <div class="ttl">Выбрано товаров:
											                      <span class="i_desc js_item_count" id="modal_basket_counter" ><?=$product_counter?></span></div>
											                    </div>
											                    <div class="item-group ns-item-group">
											                      <div class="ttl">На сумму:
											                      	<span class="js_item_price" id="modal_basket_total" ><?=$super_total?></span>р</div>
											                    </div>
															</div>


                                        <div class="modal-form__btn1">
                                        	<div class="modal-form__buy-item-ns"><a href="/cart/" class="button-border"><span>Перейти в корзину</span></a></div>
<!--                                         	<div class="modal-form__buy-item-ns"><a href="#" class="button-border button_close_form"><span>Продолжить покупки</span></a></div> -->
                                        </div>



<?php return; ?>


								                            <div class="img">
								                            	<?php if($arItem['PREVIEW_PICTURE']){ ?>
								                            		<?php $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], 
																		array('width'=>78, 'height' => 78), BX_RESIZE_IMAGE_PROPORTIONAL, true);  ?>
																		<a href="<?=$detail_url?>">
								                            				<img src="<?=$file['src']?>" alt="<?=$arItem['NAME']?>">
								                            			</a>
								                            	<?php } ?>
								                            </div>
								                            <div class="desc">
								                            	<?php

											                  		$show_price = sw_get_show_price($arItem['PRODUCT_ID'], $arItem['PRICE'], $arItem['CURRENCY']);

											                  		$total = $show_price[0] * $arItem["QUANTITY"];
											                  		$total_all = $show_price[1] * $arItem["QUANTITY"];

													                $super_total += $total;
													                $super_total_all += $total_all;
											                  	?>
								                                <div class="item-group">
								                                	<b><?=$arItem['NAME']?></b>
								                                </div>
								                                <div class="item-group">
								                                  <div class="ttl">Кол-во</div>
								                                  <div class="e-count">
								                                  	<i class="plus" data-item="<?=$arItem['ID']?>"></i>
											                  		<input type="text" data-item="<?=$arItem['ID']?>" 
											                  			value="<?=$arItem["QUANTITY"]?>" id="NUMBER_FIELD_<?=$arItem['ID']?>" />
											                  		<i class="minus" data-item="<?=$arItem['ID']?>"></i>
								                                  </div>
								                                  <i class="ico ico-remove" data-item="<?=$arItem['ID']?>"></i>
								                                </div>
								                                <span class='js_current_price' id="price_<?=$arItem['ID']?>" data-price="<?=$show_price[0]?>"><?=$show_price[0]?></span> р.</span>
								                                <div class="item-group">
								                                  <div class="ttl">Цена:</div>
								                                  <div class="i_desc">
								                                  	<span 
												                  		class='js_price total_span total_price_<?=$arItem['ID']?>'
												                  		id="total_price_<?=$arItem['ID']?>"
											                			data-full_price="<?=$arItem['PRICE']?>"
											                			data-discount_percent="<?=$arItem['DISCOUNT_PRICE_PERCENT']?>" 
											                			data-total_all="<?=$super_total_all?>"
											                			data-total="<?=$total?>"
												                  	><?=$total?></span>р.</div>
								                                </div>
								                            </div>
								                        </div>

										                <div class="modal__busket__item modal__busket__total">
										                  <div class="desc">
										                    <div class="item-group">
										                      <div class="ttl">Выбрано товаров:</div>
										                      <div class="i_desc js_item_count" id="modal_basket_counter" ><?=sizeof($arResult["GRID"]["ROWS"])?></div>
										                    </div>
										                    <div class="item-group">
										                      <div class="ttl">На сумму:</div>
										                      <div class="i_desc">
										                      	<span class="js_item_price" id="modal_basket_total" ><?=$super_total?></span>р</div>
										                    </div>
										                  </div>
										                </div>


                <?php return; ?>

      </div>
      <div class="after-bascet-section">
       
          <div class="after-bascet__wr">
            <div class="after-bascet__summ">
            	<span>Итого:</span>
            	<span><font id="basket_super_total" data-original="<?=$super_total?>" ><?=$super_total?></font> р.</span>
            </div>
            <div class="after-bascet__link">
            	<?php 
            		$form = '';
            		if(cur_page == '/personal/order/make/')
            			$form = ' form="ORDER_FORM"';
            	?>

            	<button type="subimit" <?=$form?>>Оформить заказ</a>
            </div>
          </div>
       
      </div>

<?php return; ?>

            <div class="busk_card_coupon-line clearfix">
              <div class="busk_card_cpn_left">
                <div class="clearfix card-coupon-form">
                  <div class="busk_card_cpn_left_txt">
                  	<span>Купон на скидку:</span>
                  </div>
                  <div class="busk_card_cpn_left_input">
                    <input type="text" name="coupon" id="sw_coupon_field" >
                  </div>
                  <div class="busk_card_cpn_left_btn">
                    <button  class="btn bcolor-btn" id="set_coupon" >Применить</button>
                  </div>
                </div>
               
              </div>
              <div class="busk_card_cpn_right">
              	<span>Итого: <span id="basket_super_total"><?=number_format($super_total, 0, '.', ' ')?></span> р.</span>
              </div>
               <div id="coupon_result"></div>
            </div>
            <div class="busk_card_fotter-line clearfix">
              <div class="busk_card_fotter_left">
                <div class="busk_card_fotter_left_txt">
                <span>Вернуться к выбору товаров</span></div>
                <div class="busk_card_fotter_left_btn">
                  <a href="/catalog/" class="btn m-border-btn btn-bold __cuzColor1 __cuzHoverColor1D" id="continue_shopping">
                  	<span>Продолжить покупки</span>
                  </a>
                </div>
              </div>
              <div class="busk_card_fotter_center">
                <a href="#" class="btn bcolor-btn medium btn-bold buyInOneClickTrigger" data-id="<?=$one_click_id?>"  >Быстрый заказ</a>
              </div>
              <div class="busk_card_fotter_right">
                <div class="busk_card_fotter_right_txt"><span>Полноценное оформление заказа</span></div>
                <div class="busk_card_fotter_right_btn">
                  <button type="submit"  class="btn ycolor-btn btn-bold" >Оформить заказ</button>
                </div>
              </div>
            </div>
          </div>







<?php return; ?>


						 <div class="summ__price__basket">
						 	<span class="summ__price__basket__left">В вашей корзине <i><?=sizeof($arResult["GRID"]["ROWS"])?></i> товара на сумму:</span></div>
					        <div class="summ__price__basket__rus_euro">
					        	<span class="watch_product__price right__add_summ_block__price rus__basket__price_setting">
					        		<span id="basket_super_total"><?=$arResult["allSum"]?></span> 
					        		<span class="rub watch_product__price__next">a</span>
					        	</span>
					         	<?php $eur = CCurrencyRates::ConvertCurrency($arResult["allSum"], "RUB", "EUR"); ?>	
					        	<span class="watch_product__price_euro watch_product__price_euro__cheapter right__add_summ_block__euro euro__basket__price_setting">
					        		<span id="eur_basket_super_total"><?=round($eur)?></span> 
					        		<span class="rub watch_product__price__next_euro">&#8364;</span>
					        	</span>
					        </div>
					        <button type="submit" onclick="checkOut();" class="basket__checkout">Оформить заказ</button>
					        </div>
					      </div>
					    </div>
					




<div class="cart-product-list-container">
											<ul class="reset-list cart-product-list busket-place-content" id="basket_items">
												<li class="cpl__header clearfix hidden-xs">
													<div class="cpl__product-descr-head fl">
														<span>Товар</span>
													</div>
													<div class="cpl__product-quantity-head fl">
														<span>Количество</span>														
													</div>
													<div class="cpl__product-price-head fl">
														<span>Цена за единицу</span>
													</div>
													<div class="cpl__product-price-total-head">
														<span>Итоговая цена</span>
													</div>
												</li>
												<?
												$discount_price = 0;


												foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
													$arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
													if ($arHeader["name"] == '')
														$arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
													$arHeaders[] = $arHeader["id"];

													// remember which values should be shown not in the separate columns, but inside other columns
													if (in_array($arHeader["id"], array("TYPE")))
													{
														$bPriceType = true;
														continue;
													}
													elseif ($arHeader["id"] == "PROPS")
													{
														$bPropsColumn = true;
														continue;
													}
													elseif ($arHeader["id"] == "DELAY")
													{
														$bDelayColumn = true;
														continue;
													}
													elseif ($arHeader["id"] == "DELETE")
													{
														$bDeleteColumn = true;
														continue;
													}
													elseif ($arHeader["id"] == "WEIGHT")
													{
														$bWeightColumn = true;
													}

													if ($arHeader["id"] == "NAME"):
													?>
														<!-- <td class="item" colspan="2" id="col_<?=$arHeader["id"];?>"> -->
													<?
													elseif ($arHeader["id"] == "PRICE"):
													?>
														<!-- <td class="price" id="col_<?=$arHeader["id"];?>"> -->
													<?
													else:
													?>
														<!-- <td class="custom" id="col_<?=$arHeader["id"];?>"> -->
													<?
													endif;
													?>
														
														<!-- </td> -->
												<?
												endforeach;

												if ($bDeleteColumn || $bDelayColumn):
												?>
													<!-- <td class="custom"></td> -->
												<?
												endif;
												?>
													<?php foreach ($arResult["GRID"]["ROWS"] as $key => $arItem): ?>
														<?php
															$ar_res = CCatalogProduct::GetByID($arItem["PRODUCT_ID"]); 
															
														?>



												<li class="clearfix basket_item" id="basket_item_<?=$arItem["ID"]?>">
													<div class="cpl__product-descr fl">
														<div class="cpl__image-container fl">
															<?php
																if($arItem['PREVIEW_PICTURE']){ 
															?>
																<?php $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], 
																array('width'=>205, 'height' => 192), BX_RESIZE_IMAGE_EXACT, true);  ?>
																<a href="<?=$arItem["DETAIL_PAGE_URL"] ?>">
																	<img src="<?=$file['src']?>" alt="" title="">
																</a>
															<?php } ?>
														</div>
														<div class="cpl__product-options fl opened">
															<a href="<?=$arItem["DETAIL_PAGE_URL"] ?>" class="cpl__product-title"><?=$arItem['NAME']?></a>


															<button type="button" class="button-default cpl__toggle-addition-services visible-xs">
																<span class="button-text">доп. услуги</span>
																<span class="icon-container">
																	<i class="icon icon-arrow-white-up icon-active"></i>
																	<i class="icon icon-arrow-grey-down"></i>
																</span>
															</button>
															<div class="cpl__checkboxes-wrapper">
																<?php
										                          $arSelect = Array("ID", "NAME", 'IBLOCK_ID', "PROPERTY_*");
										                          $arFilter = Array("IBLOCK_ID"=>$arItem['IBLOCK_ID'], "ACTIVE"=>"Y", 'ID' => $arItem['PRODUCT_ID']);
										                          $res = CIBlockElement::GetList(
										                              Array("SORT" => "ASC"), 
										                              $arFilter, false, Array("nPageSize"=>1), $arSelect);
										                          $total = $res -> SelectedRowsCount();
										                          while($ob = $res->GetNextElement()){
										                            $arFields = $ob->GetFields();
										                            $arProps = $ob->GetProperties(false, array('CODE' => 'CHAR_%', '!PROPERTY_VALUE' => ''));
										                          ?>
										                            <ul class="cpl__checkboxes reset-list characteristics-list">
										                            <?php
										                            foreach($arProps as $prop){

										                              if($prop['VALUE'] == '' || $prop['CODE'] == 'CHAR_IN_STOCK')
										                                continue;

										                                $table = $prop['USER_TYPE_SETTINGS']['TABLE_NAME'];
										                              ?>
										                               <li class="clearfix">
										                                <span class="key"><?=$prop['NAME']?>:</span>
										                                <span class="value"><?=sw_get_value_by_id($table, $prop['VALUE'])?></span>
										                              </li>
										                              <?php
										                            } 
										                            ?></ul><?php
										                          }
										                        ?>
																
															</div>
														</div>
													</div>
													<div class="cpl__product-quantity fl">
														<div class="dib">
															<div class="d-table">
																<div class="cell">
																	<div class="cpl__quantity-block quantity-container">
																		<button type="button" class="button-default dec" data-item="<?=$arItem['ID']?>">
																			<span class="icon-container">
																				<i  class="icon icon-decrement-down hidden-xs hidden-sm"></i>
																				<i  class="icon icon-decrement-down-hover icon-hover hidden-xs hidden-sm"></i>
																				<i  class="icon icon-decrement-minus hidden-md hidden-lg"></i>
																				<i  class="icon icon-decrement-minus-hover icon-hover hidden-md hidden-lg"></i>
																			</span>
																		</button>

																		<?
																		$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
																		$max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
																		$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
																		$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
																		?>
														              
																		
																		<input 
																			type="number" 
																			id="NUMBER_FIELD_<?=$arItem['ID']?>" 
																			value="<?=$arItem["QUANTITY"]?>" 
																			placeholder="" 
																			
																			step="<?=$ratio?>"
																			
																			data-item="<?=$arItem['ID']?>"
																			class="quantity">

																		<button type="button" class="button-default inc" data-item="<?=$arItem['ID']?>">
																			<span class="icon-container" >
																				<i class="icon icon-increment-up hidden-xs hidden-sm"></i>
																				<i class="icon icon-increment-up-hover icon-hover hidden-xs hidden-sm"></i>
																				<i class="icon icon-increment-plus hidden-md hidden-lg"></i>
																				<i class="icon icon-increment-plus-hover icon-hover hidden-md hidden-lg"></i>
																			</span>
																		</button>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="cpl__product-price fl">
														<div class="dib">
															<div class="d-table">
																<div class="cell" >
																	<?php
																		$discount_price += (float) $arItem['DISCOUNT_PRICE'] * (int) $arItem["QUANTITY"];
																		$show_price = sw_get_show_price($arItem['PRODUCT_ID'], $arItem['FULL_PRICE'], $arItem['CURRENCY']);
																		
																	?>
																	<span 
																		id="price_<?=$arItem['ID']?>"><?=$arItem['PRICE']?> руб.</span>
																</div>
															</div>
														</div>
													</div>
													<div class="cpl__product-price-total">
														<div class="dib">
															<div class="d-table">
																<div class="cell" >
																	<span 
																		class="total_span" 
																		data-full_price="<?=$arItem['FULL_PRICE']?>"
																		data-discount_percent="<?=$arItem['DISCOUNT_PRICE_PERCENT']?>" 
																		id="total_price_<?=$arItem['ID']?>"><?=($arItem['PRICE'] * $arItem["QUANTITY"])?> руб.</span>
																</div>
															</div>
														</div>
													</div>
													<button type="button" class="button-default cpl__remove-product" data-item="<?=$arItem['ID']?>" >
														<i class="icon icon-remove hidden-sm"></i>
														<i class="icon icon-close-modal-sm hidden-md higgen-lg hidden-xs"></i>
													</button>
												</li>
												<?php endforeach; ?>

											</ul>
										</div>
										<div class="cart-totals-block">
											<div class="col-lg-12 col-md-12 ">
													<div class="cart-totals-block__promo">
														<div class="cart-totals-block__promo-title">
															<span>Акции по промокоду</span>
															<button type="button" class="help-info" data-toggle="tooltip" data-placement="top" title="Some help info">?</button>
														</div>
														<div class="cart-totals-block__inputs">
																
																<input type="text" 
																	name="coupon" 
																	id="sw_coupon_field"
																	placeholder="Введите промокод" value="" class="promo-input">
																<button type="button" id="set_coupon" class="button-default button-green">Применить</button>
																<div id="coupon_result"></div>
																
															
														</div>
													</div>
													<?php
													$dbProductDiscounts = CCatalogDiscount::GetList(
												    array("SORT" => "ASC"),
												    array(
												           
												        ),
												    false,
												    false,
												    array(
												            "ID", "SITE_ID", "ACTIVE", "ACTIVE_FROM", "ACTIVE_TO", 
												            "RENEWAL", "NAME", "SORT", "MAX_DISCOUNT", "VALUE_TYPE", 'NOTES',
												    		"VALUE", "CURRENCY", "PRODUCT_ID"
												        )
												    );

												    $total_promos = $dbProductDiscounts -> SelectedRowsCount();
												    if($total_promos > 0){
													?>
													<div class="cart-totals-block__actions">
														<div class="cart-totals-block__actions-title">
															<span>Действущие акции</span>
														</div>
														<div class="cart-totals-block__actions-list">
															<ul class="reset-list">
																<?php
																	while ($arProductDiscounts = $dbProductDiscounts->Fetch()){
		   			
																?>
																<li>
																	<label class="custom-checkbox">
																		<input type="checkbox">
																		<!-- <span class="icon-container">
																			<i class="icon icon-checkbox-empty-small hidden-xs hidden-sm"></i>
																			<i class="icon icon-checkbox-empty hidden-md hidden-lg"></i>
																			<i class="icon icon-checkbox-checked-pink-small hidden-xs hidden-sm icon-checked"></i>
																			<i class="icon icon-checkbox-checked-pink hidden-md hidden-lg icon-checked"></i>
																		</span> -->
																		<span class="label-text">
																			<span><?=$arProductDiscounts['NAME']?></span>
																		</span>
																	</label>
																	<?php if($arProductDiscounts['NOTES'] != ''){ ?>
																		<button type="button" class="help-info" data-toggle="tooltip" data-placement="top" title="<?=$arProductDiscounts['NOTES']?>">?</button>
																	<?php } ?>
																</li>
																<?php } ?>
															</ul>
														</div>
														<?php if(sizeof($arResult["GRID"]["ROWS"]) > 1){ ?>
														<br/>
														<div class="totals-info">
															<p>В корзине несколько товаров. Мененджер сообщит конечную стоимость дополнительных услуг при подтверждении Вашего заказа</p>
														</div>
														<?php } ?>
													</div>

											</div>

											<?php } ?>

											<div class="col-lg-12 col-md-12">

													<div class="cart-totals-block__total-price clearfix">
														
														<div class="total-price-container">
															<?php if($discount_price > 0){ 
																///var_dump($arResult); 
															?>
															<div class="discount">
																<div class="key">Скидка:</div>
																<div class="value" id="basket_super_discount"><?=$discount_price?> руб.</div>
															</div>
															<?php } ?>
															<div class="total-ptice">
																<div class="key">Итого:</div>
																<div class="value" id="basket_super_total"><?=$arResult["allSum_FORMATED"]?></div>
															</div>
														</div>
													</div>

													<div class="cart-totals-block__button-container ">
														<input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode($arHeaders, ","))?>" />
														<input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
														<input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
														<input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
														<input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
														<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
														<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
														<input type="hidden" id="coupon_approved" value="N" />
														<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
														
														<div class="cart-order-form__footer ">
															<div class="button-container">
																<button type="submit" class="button-default button-green" onclick="checkOut();">Оформить заказ</button>
															</div>
														</div>
													</div>
											</div>
										</div>


										
          

										<?php return; ?>
<tr id="<?=$arItem["ID"]?>" >
              <td>
              <div class="busket-place-item <?php if($ar_res['QUANTITY'] == 0) echo 'not-in-stock';?> <?php if(($key ) == sizeof($arResult["GRID"]["ROWS"])) echo 'last-one'; ?>">
              <div class="img-place">
              	<?
										if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
											$url = $arItem["PREVIEW_PICTURE_SRC"];
										elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
											$url = $arItem["DETAIL_PICTURE_SRC"];
										else:
											$url = $templateFolder."/images/no_photo.png";
										endif;
										?>

                <a href="<?=$arItem["DETAIL_PAGE_URL"] ?>">
                  <img src="<?=$url?>" height="100" width="100" alt="">
                </a>
              </div>
              <div class="info-place">
                <a href="<?=$arItem["DETAIL_PAGE_URL"] ?>" class="title"><?=$arItem['NAME']?></a>
                <?php
                	$element = null;

                	$arSelect = Array("ID", "NAME", "PROPERTY_ARTICUL", "CATALOG_GROUP_1", "CATALOG_PRICE_1");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
					$arFilter = Array("IBLOCK_ID"=>$ar_res['ID'], "ACTIVE"=>"Y");
					$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>500), $arSelect);
					$total = $res -> SelectedRowsCount();
					while($ob = $res->GetNextElement()){
						$arFields = $ob->GetFields();
						$element = $arFields;
					}
                	//var_dump($arItem);
                	if($arItem['PROPERTY_ARTICUL_VALUE']){ 
                ?>
                	<span class="art">№ <?=$arItem['PROPERTY_ARTICUL_VALUE']?></span>
                <?php } ?>
                <?php if($arItem['PROPERTY_COLOR_VALUE']){ ?>
                	<span class="opt">Цвет: <b><?=$arItem['PROPERTY_COLOR_VALUE']?></b></span>
                <?php } ?>
                <?php if($arItem['PROPERTY_MATERIAL_VALUE']){ ?>
                	<span class="opt">Материал подкладки: <b><?=$arItem['PROPERTY_MATERIAL_VALUE']?></b></span>
                <?php } ?>
                <?php if($ar_res['QUANTITY'] == 0){ ?>
                	<br/>
	                <span class="not-in-stock-msg">
	                  Нет в наличии
	                </span>
                <?php } ?>
              </div>
              <div class="price-block">
              	<?php
            		//discounts
					$arDiscounts = CCatalogDiscount::GetDiscountByProduct(
				        $arItem['PRODUCT_ID'],
				        $USER->GetUserGroupArray(),
				        "N",
				        array(),
				        SITE_ID
				    );
				    
				    $discont_price = CCatalogProduct::CountPriceWithDiscount(
					  $arItem['BASE_PRICE'],
					  'RUB',
					  $arDiscounts
					);

					
					$price = (float) $arItem['BASE_PRICE'];
					$show_price = $price;
					
					if($price != $discont_price)
						$show_price = $discont_price;

					//var_dump($arElement); 
					

            	?>
                <span class="price"><?=number_format($show_price, 0, '.', ' ')?> руб.</span>
                <?php if($price != $discont_price){ ?>
                	<span class="old-price"><?=number_format($price, 0, '.', ' ')?> руб.</span>
                <?php } ?>
              </div>
              <div class="plusser-block">
              	<?php if($ar_res['QUANTITY'] == 1){ ?>
	              	<div class="last-one-msg">
	                  Последний
	                </div>
                <?php }else{ ?>
                	<?
					$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
					$max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
					$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
					$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
					?>
	                <span class="plusser-item minus" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>);"></span>
	                
					<input
						type="hidden"
						size="3"
						id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
						name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
						size="2"
						maxlength="18"
						min="0"
						<?=$max?>
						step="<?=$ratio?>"
						style="max-width: 50px"
						value="<?=$arItem["QUANTITY"]?>"
						onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>);"
					>
					

					
					<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" 
						name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" 

						/>

	                <span class="number" id="NUMBER_FIELD_<?=$arItem['ID']?>"><?=$arItem['QUANTITY']?></span>
	                <span class="plusser-item plus"  onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>);"></span>
	            <?php } ?>
              </div>
              <div class="total-price-block">
                <span id="sum_<?=$arItem["ID"]?>"><?=$arItem["SUM"]?></span>
              </div>
              <button class="sil-btn">
              	<i class="ico close-ico"></i>
              	<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" class="hidden-xs hidden-sm" >Удалить из корзины</a>
              </button>
              </div>
              <td>
            </tr>






<table class="busket-place-content" id="basket_items">
	<?
		foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
			$arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
			if ($arHeader["name"] == '')
				$arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
			$arHeaders[] = $arHeader["id"];

			// remember which values should be shown not in the separate columns, but inside other columns
			if (in_array($arHeader["id"], array("TYPE")))
			{
				$bPriceType = true;
				continue;
			}
			elseif ($arHeader["id"] == "PROPS")
			{
				$bPropsColumn = true;
				continue;
			}
			elseif ($arHeader["id"] == "DELAY")
			{
				$bDelayColumn = true;
				continue;
			}
			elseif ($arHeader["id"] == "DELETE")
			{
				$bDeleteColumn = true;
				continue;
			}
			elseif ($arHeader["id"] == "WEIGHT")
			{
				$bWeightColumn = true;
			}

			if ($arHeader["id"] == "NAME"):
			?>
				<!-- <td class="item" colspan="2" id="col_<?=$arHeader["id"];?>"> -->
			<?
			elseif ($arHeader["id"] == "PRICE"):
			?>
				<!-- <td class="price" id="col_<?=$arHeader["id"];?>"> -->
			<?
			else:
			?>
				<!-- <td class="custom" id="col_<?=$arHeader["id"];?>"> -->
			<?
			endif;
			?>
				
				<!-- </td> -->
		<?
		endforeach;

		if ($bDeleteColumn || $bDelayColumn):
		?>
			<!-- <td class="custom"></td> -->
		<?
		endif;
		?>
			<?php foreach ($arResult["GRID"]["ROWS"] as $key => $arItem): ?>
				<?php
					$ar_res = CCatalogProduct::GetByID($arItem["PRODUCT_ID"]); 
					
				?>
            <tr id="<?=$arItem["ID"]?>" >
              <td>
              <div class="busket-place-item <?php if($ar_res['QUANTITY'] == 0) echo 'not-in-stock';?> <?php if(($key ) == sizeof($arResult["GRID"]["ROWS"])) echo 'last-one'; ?>">
              <div class="img-place">
              	<?
										if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
											$url = $arItem["PREVIEW_PICTURE_SRC"];
										elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
											$url = $arItem["DETAIL_PICTURE_SRC"];
										else:
											$url = $templateFolder."/images/no_photo.png";
										endif;
										?>

                <a href="<?=$arItem["DETAIL_PAGE_URL"] ?>">
                  <img src="<?=$url?>" height="100" width="100" alt="">
                </a>
              </div>
              <div class="info-place">
                <a href="<?=$arItem["DETAIL_PAGE_URL"] ?>" class="title"><?=$arItem['NAME']?></a>
                <?php
                	$element = null;

                	$arSelect = Array("ID", "NAME", "PROPERTY_ARTICUL", "CATALOG_GROUP_1", "CATALOG_PRICE_1");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
					$arFilter = Array("IBLOCK_ID"=>$ar_res['ID'], "ACTIVE"=>"Y");
					$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>500), $arSelect);
					$total = $res -> SelectedRowsCount();
					while($ob = $res->GetNextElement()){
						$arFields = $ob->GetFields();
						$element = $arFields;
					}
                	//var_dump($arItem);
                	if($arItem['PROPERTY_ARTICUL_VALUE']){ 
                ?>
                	<span class="art">№ <?=$arItem['PROPERTY_ARTICUL_VALUE']?></span>
                <?php } ?>
                <?php if($arItem['PROPERTY_COLOR_VALUE']){ ?>
                	<span class="opt">Цвет: <b><?=$arItem['PROPERTY_COLOR_VALUE']?></b></span>
                <?php } ?>
                <?php if($arItem['PROPERTY_MATERIAL_VALUE']){ ?>
                	<span class="opt">Материал подкладки: <b><?=$arItem['PROPERTY_MATERIAL_VALUE']?></b></span>
                <?php } ?>
                <?php if($ar_res['QUANTITY'] == 0){ ?>
                	<br/>
	                <span class="not-in-stock-msg">
	                  Нет в наличии
	                </span>
                <?php } ?>
              </div>
              <div class="price-block">
              	<?php
            		//discounts
					$arDiscounts = CCatalogDiscount::GetDiscountByProduct(
				        $arItem['PRODUCT_ID'],
				        $USER->GetUserGroupArray(),
				        "N",
				        array(),
				        SITE_ID
				    );
				    
				    $discont_price = CCatalogProduct::CountPriceWithDiscount(
					  $arItem['BASE_PRICE'],
					  'RUB',
					  $arDiscounts
					);

					
					$price = (float) $arItem['BASE_PRICE'];
					$show_price = $price;
					
					if($price != $discont_price)
						$show_price = $discont_price;

					//var_dump($arElement); 
					

            	?>
                <span class="price"><?=number_format($show_price, 0, '.', ' ')?> руб.</span>
                <?php if($price != $discont_price){ ?>
                	<span class="old-price"><?=number_format($price, 0, '.', ' ')?> руб.</span>
                <?php } ?>
              </div>
              <div class="plusser-block">
              	<?php if($ar_res['QUANTITY'] == 1){ ?>
	              	<div class="last-one-msg">
	                  Последний
	                </div>
                <?php }else{ ?>
                	<?
					$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
					$max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
					$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
					$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
					?>
	                <span class="plusser-item minus" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>);"></span>
	                
					<input
						type="hidden"
						size="3"
						id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
						name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
						size="2"
						maxlength="18"
						min="0"
						<?=$max?>
						step="<?=$ratio?>"
						style="max-width: 50px"
						value="<?=$arItem["QUANTITY"]?>"
						onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>);"
					>
					

					
					<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" 
						name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" 

						/>

	                <span class="number" id="NUMBER_FIELD_<?=$arItem['ID']?>"><?=$arItem['QUANTITY']?></span>
	                <span class="plusser-item plus"  onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>);"></span>
	            <?php } ?>
              </div>
              <div class="total-price-block">
                <span id="sum_<?=$arItem["ID"]?>"><?=$arItem["SUM"]?></span>
              </div>
              <button class="sil-btn">
              	<i class="ico close-ico"></i>
              	<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" class="hidden-xs hidden-sm" >Удалить из корзины</a>
              </button>
              </div>
              <td>
            </tr>

       		<?php endforeach; ?>
       		<input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode($arHeaders, ","))?>" />
			<input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
			<input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
			<input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
			<input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
			<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
			<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
			<input type="hidden" id="coupon_approved" value="N" />
			<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
          </table>
           <div class="busket-place-footer">
            <div class="totals-wrap">
              <div class="totals-block">
                <div class="line">
                  <span class="text">Товаров в корзине:</span>
                  <i class="ico close-ico"></i>
                  <span class="ttl-text"><?=sizeof($arResult["GRID"]["ROWS"])?></span>
                </div>
                <div class="line">
                  <span class="text">Стоимость покупки:</span>
                  <i class="ico close-ico"></i>
                  <span class="ttl-text big" id="allSum_FORMATED"><?=$arResult["allSum_FORMATED"]?></span>
                </div>
              </div>
            </div>
            <div class="btn-wrap">
              <button class="or-btn" onclick="checkOut();" >Оформить заказ</button>
            </div>
          </div>

          <?php return; ?>



<div class="bx_ordercart_order_pay_right">
			<table class="bx_ordercart_order_sum">
				<?if ($bWeightColumn):?>
					<tr>
						<td class="custom_t1"><?=GetMessage("SALE_TOTAL_WEIGHT")?></td>
						<td class="custom_t2" id="allWeight_FORMATED"><?=$arResult["allWeight_FORMATED"]?></td>
					</tr>
				<?endif;?>
				<?if ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y"):?>
					<tr>
						<td><?echo GetMessage('SALE_VAT_EXCLUDED')?></td>
						<td id="allSum_wVAT_FORMATED"><?=$arResult["allSum_wVAT_FORMATED"]?></td>
					</tr>
					<tr>
						<td><?echo GetMessage('SALE_VAT_INCLUDED')?></td>
						<td id="allVATSum_FORMATED"><?=$arResult["allVATSum_FORMATED"]?></td>
					</tr>
				<?endif;?>

					<tr>
						<td class="fwb"><?=GetMessage("SALE_TOTAL")?></td>
						<td class="fwb" id="allSum_FORMATED"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></td>
					</tr>
					<tr>
						<td class="custom_t1"></td>
						<td class="custom_t2" style="text-decoration:line-through; color:#828282;" id="PRICE_WITHOUT_DISCOUNT">
							<?if (floatval($arResult["DISCOUNT_PRICE_ALL"]) > 0):?>
								<?=$arResult["PRICE_WITHOUT_DISCOUNT"]?>
							<?endif;?>
						</td>
					</tr>

			</table>
			<div style="clear:both;"></div>
		</div>
		<div style="clear:both;"></div>

		<div class="bx_ordercart_order_pay_center">

			<?if ($arParams["USE_PREPAYMENT"] == "Y" && strlen($arResult["PREPAY_BUTTON"]) > 0):?>
				<?=$arResult["PREPAY_BUTTON"]?>
				<span><?=GetMessage("SALE_OR")?></span>
			<?endif;?>

			<a class="checkout"><?=GetMessage("SALE_ORDER")?></a>
		</div>





<div id="basket_items_list">
	<div class="bx_ordercart_order_table_container">
		<table id="basket_items">
			<thead>
				<tr>
					<td class="margin"></td>
					<?
					foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
						$arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
						if ($arHeader["name"] == '')
							$arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
						$arHeaders[] = $arHeader["id"];

						// remember which values should be shown not in the separate columns, but inside other columns
						if (in_array($arHeader["id"], array("TYPE")))
						{
							$bPriceType = true;
							continue;
						}
						elseif ($arHeader["id"] == "PROPS")
						{
							$bPropsColumn = true;
							continue;
						}
						elseif ($arHeader["id"] == "DELAY")
						{
							$bDelayColumn = true;
							continue;
						}
						elseif ($arHeader["id"] == "DELETE")
						{
							$bDeleteColumn = true;
							continue;
						}
						elseif ($arHeader["id"] == "WEIGHT")
						{
							$bWeightColumn = true;
						}

						if ($arHeader["id"] == "NAME"):
						?>
							<td class="item" colspan="2" id="col_<?=$arHeader["id"];?>">
						<?
						elseif ($arHeader["id"] == "PRICE"):
						?>
							<td class="price" id="col_<?=$arHeader["id"];?>">
						<?
						else:
						?>
							<td class="custom" id="col_<?=$arHeader["id"];?>">
						<?
						endif;
						?>
							<?=$arHeader["name"]; ?>
							</td>
					<?
					endforeach;

					if ($bDeleteColumn || $bDelayColumn):
					?>
						<td class="custom"></td>
					<?
					endif;
					?>
						<td class="margin"></td>
				</tr>
			</thead>

			<tbody>
				<?
				foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

					if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
				?>
					<tr id="<?=$arItem["ID"]?>">
						<td class="margin"></td>
						<?
						foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

							if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE", "TYPE"))) // some values are not shown in the columns in this template
								continue;

							if ($arHeader["id"] == "NAME"):
							?>
								<td class="itemphoto">
									<div class="bx_ordercart_photo_container">
										<?
										if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
											$url = $arItem["PREVIEW_PICTURE_SRC"];
										elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
											$url = $arItem["DETAIL_PICTURE_SRC"];
										else:
											$url = $templateFolder."/images/no_photo.png";
										endif;
										?>

										<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
											<div class="bx_ordercart_photo" style="background-image:url('<?=$url?>')"></div>
										<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
									</div>
									<?
									if (!empty($arItem["BRAND"])):
									?>
									<div class="bx_ordercart_brand">
										<img alt="" src="<?=$arItem["BRAND"]?>" />
									</div>
									<?
									endif;
									?>
								</td>
								<td class="item">
									<h2 class="bx_ordercart_itemtitle">
										<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
											<?=$arItem["NAME"]?>
										<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
									</h2>
									<div class="bx_ordercart_itemart">
										<?
										if ($bPropsColumn):
											foreach ($arItem["PROPS"] as $val):

												if (is_array($arItem["SKU_DATA"]))
												{
													$bSkip = false;
													foreach ($arItem["SKU_DATA"] as $propId => $arProp)
													{
														if ($arProp["CODE"] == $val["CODE"])
														{
															$bSkip = true;
															break;
														}
													}
													if ($bSkip)
														continue;
												}

												echo $val["NAME"].":&nbsp;<span>".$val["VALUE"]."<span><br/>";
											endforeach;
										endif;
										?>
									</div>
									<?
									if (is_array($arItem["SKU_DATA"]) && !empty($arItem["SKU_DATA"])):
										foreach ($arItem["SKU_DATA"] as $propId => $arProp):

											// if property contains images or values
											$isImgProperty = false;
											if (array_key_exists('VALUES', $arProp) && is_array($arProp["VALUES"]) && !empty($arProp["VALUES"]))
											{
												foreach ($arProp["VALUES"] as $id => $arVal)
												{
													if (isset($arVal["PICT"]) && !empty($arVal["PICT"]) && is_array($arVal["PICT"])
														&& isset($arVal["PICT"]['SRC']) && !empty($arVal["PICT"]['SRC']))
													{
														$isImgProperty = true;
														break;
													}
												}
											}
											$countValues = count($arProp["VALUES"]);
											$full = ($countValues > 5) ? "full" : "";

											if ($isImgProperty): // iblock element relation property
											?>
												<div class="bx_item_detail_scu_small_noadaptive <?=$full?>">

													<span class="bx_item_section_name_gray">
														<?=$arProp["NAME"]?>:
													</span>

													<div class="bx_scu_scroller_container">

														<div class="bx_scu">
															<ul id="prop_<?=$arProp["CODE"]?>_<?=$arItem["ID"]?>"
																style="width: 200%; margin-left:0%;"
																class="sku_prop_list"
																>
																<?
																foreach ($arProp["VALUES"] as $valueId => $arSkuValue):

																	$selected = "";
																	foreach ($arItem["PROPS"] as $arItemProp):
																		if ($arItemProp["CODE"] == $arItem["SKU_DATA"][$propId]["CODE"])
																		{
																			if ($arItemProp["VALUE"] == $arSkuValue["NAME"] || $arItemProp["VALUE"] == $arSkuValue["XML_ID"])
																				$selected = "bx_active";
																		}
																	endforeach;
																?>
																	<li style="width:10%;"
																		class="sku_prop <?=$selected?>"
																		data-value-id="<?=$arSkuValue["XML_ID"]?>"
																		data-element="<?=$arItem["ID"]?>"
																		data-property="<?=$arProp["CODE"]?>"
																		>
																		<a href="javascript:void(0);">
																			<span style="background-image:url(<?=$arSkuValue["PICT"]["SRC"]?>)"></span>
																		</a>
																	</li>
																<?
																endforeach;
																?>
															</ul>
														</div>

														<div class="bx_slide_left" onclick="leftScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
														<div class="bx_slide_right" onclick="rightScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
													</div>

												</div>
											<?
											else:
											?>
												<div class="bx_item_detail_size_small_noadaptive <?=$full?>">

													<span class="bx_item_section_name_gray">
														<?=$arProp["NAME"]?>:
													</span>

													<div class="bx_size_scroller_container">
														<div class="bx_size">
															<ul id="prop_<?=$arProp["CODE"]?>_<?=$arItem["ID"]?>"
																style="width: 200%; margin-left:0%;"
																class="sku_prop_list"
																>
																<?
																foreach ($arProp["VALUES"] as $valueId => $arSkuValue):

																	$selected = "";
																	foreach ($arItem["PROPS"] as $arItemProp):
																		if ($arItemProp["CODE"] == $arItem["SKU_DATA"][$propId]["CODE"])
																		{
																			if ($arItemProp["VALUE"] == $arSkuValue["NAME"])
																				$selected = "bx_active";
																		}
																	endforeach;
																?>
																	<li style="width:10%;"
																		class="sku_prop <?=$selected?>"
																		data-value-id="<?=$arSkuValue["NAME"]?>"
																		data-element="<?=$arItem["ID"]?>"
																		data-property="<?=$arProp["CODE"]?>"
																		>
																		<a href="javascript:void(0);"><?=$arSkuValue["NAME"]?></a>
																	</li>
																<?
																endforeach;
																?>
															</ul>
														</div>
														<div class="bx_slide_left" onclick="leftScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
														<div class="bx_slide_right" onclick="rightScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
													</div>

												</div>
											<?
											endif;
										endforeach;
									endif;
									?>
								</td>
							<?
							elseif ($arHeader["id"] == "QUANTITY"):
							?>
								<td class="custom">
									<span><?=$arHeader["name"]; ?>:</span>
									<div class="centered">
										<table cellspacing="0" cellpadding="0" class="counter">
											<tr>
												<td>
													<?
													$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
													$max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
													$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
													$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
													?>
													<input
														type="text"
														size="3"
														id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
														name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
														size="2"
														maxlength="18"
														min="0"
														<?=$max?>
														step="<?=$ratio?>"
														style="max-width: 50px"
														value="<?=$arItem["QUANTITY"]?>"
														onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)"
													>
												</td>
												<?
												if (!isset($arItem["MEASURE_RATIO"]))
												{
													$arItem["MEASURE_RATIO"] = 1;
												}

												if (
													floatval($arItem["MEASURE_RATIO"]) != 0
												):
												?>
													<td id="basket_quantity_control">
														<div class="basket_quantity_control">
															<a href="javascript:void(0);" class="plus" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>);"></a>
															<a href="javascript:void(0);" class="minus" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>);"></a>
														</div>
													</td>
												<?
												endif;
												if (isset($arItem["MEASURE_TEXT"]))
												{
													?>
														<td style="text-align: left"><?=$arItem["MEASURE_TEXT"]?></td>
													<?
												}
												?>
											</tr>
										</table>
									</div>
									<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
								</td>
							<?
							elseif ($arHeader["id"] == "PRICE"):
							?>
								<td class="price">
										<div class="current_price" id="current_price_<?=$arItem["ID"]?>">
											<?=$arItem["PRICE_FORMATED"]?>
										</div>
										<div class="old_price" id="old_price_<?=$arItem["ID"]?>">
											<?if (floatval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0):?>
												<?=$arItem["FULL_PRICE_FORMATED"]?>
											<?endif;?>
										</div>

									<?if ($bPriceType && strlen($arItem["NOTES"]) > 0):?>
										<div class="type_price"><?=GetMessage("SALE_TYPE")?></div>
										<div class="type_price_value"><?=$arItem["NOTES"]?></div>
									<?endif;?>
								</td>
							<?
							elseif ($arHeader["id"] == "DISCOUNT"):
							?>
								<td class="custom">
									<span><?=$arHeader["name"]; ?>:</span>
									<div id="discount_value_<?=$arItem["ID"]?>"><?=$arItem["DISCOUNT_PRICE_PERCENT_FORMATED"]?></div>
								</td>
							<?
							elseif ($arHeader["id"] == "WEIGHT"):
							?>
								<td class="custom">
									<span><?=$arHeader["name"]; ?>:</span>
									<?=$arItem["WEIGHT_FORMATED"]?>
								</td>
							<?
							else:
							?>
								<td class="custom">
									<span><?=$arHeader["name"]; ?>:</span>
									<?
									if ($arHeader["id"] == "SUM"):
									?>
										<div id="sum_<?=$arItem["ID"]?>">
									<?
									endif;

									echo $arItem[$arHeader["id"]];

									if ($arHeader["id"] == "SUM"):
									?>
										</div>
									<?
									endif;
									?>
								</td>
							<?
							endif;
						endforeach;

						if ($bDelayColumn || $bDeleteColumn):
						?>
							<td class="control">
								<?
								if ($bDeleteColumn):
								?>
									<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>"><?=GetMessage("SALE_DELETE")?></a><br />
								<?
								endif;
								if ($bDelayColumn):
								?>
									<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delay"])?>"><?=GetMessage("SALE_DELAY")?></a>
								<?
								endif;
								?>
							</td>
						<?
						endif;
						?>
							<td class="margin"></td>
					</tr>
					<?
					endif;
				endforeach;
				?>
			</tbody>
		</table>
	</div>
	<input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode($arHeaders, ","))?>" />
	<input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
	<input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
	<input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
	<input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="coupon_approved" value="N" />
	<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />

	<div class="bx_ordercart_order_pay">

		<div class="bx_ordercart_order_pay_left">
			<div class="bx_ordercart_coupon">
				<?
				if ($arParams["HIDE_COUPON"] != "Y"):

					$couponClass = "";
					if (array_key_exists('VALID_COUPON', $arResult))
					{
						$couponClass = ($arResult["VALID_COUPON"] === true) ? "good" : "bad";
					}
					elseif (array_key_exists('COUPON', $arResult) && !empty($arResult["COUPON"]))
					{
						$couponClass = "good";
					}

				?>
					<span><?=GetMessage("STB_COUPON_PROMT")?></span>
					<input type="text" id="coupon" name="COUPON" value="<?=$arResult["COUPON"]?>" onchange="enterCoupon();" size="21" class="<?=$couponClass?>">
				<?else:?>
					&nbsp;
				<?endif;?>
			</div>
		</div>

		<div class="bx_ordercart_order_pay_right">
			<table class="bx_ordercart_order_sum">
				<?if ($bWeightColumn):?>
					<tr>
						<td class="custom_t1"><?=GetMessage("SALE_TOTAL_WEIGHT")?></td>
						<td class="custom_t2" id="allWeight_FORMATED"><?=$arResult["allWeight_FORMATED"]?></td>
					</tr>
				<?endif;?>
				<?if ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y"):?>
					<tr>
						<td><?echo GetMessage('SALE_VAT_EXCLUDED')?></td>
						<td id="allSum_wVAT_FORMATED"><?=$arResult["allSum_wVAT_FORMATED"]?></td>
					</tr>
					<tr>
						<td><?echo GetMessage('SALE_VAT_INCLUDED')?></td>
						<td id="allVATSum_FORMATED"><?=$arResult["allVATSum_FORMATED"]?></td>
					</tr>
				<?endif;?>

					<tr>
						<td class="fwb"><?=GetMessage("SALE_TOTAL")?></td>
						<td class="fwb" id="allSum_FORMATED"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></td>
					</tr>
					<tr>
						<td class="custom_t1"></td>
						<td class="custom_t2" style="text-decoration:line-through; color:#828282;" id="PRICE_WITHOUT_DISCOUNT">
							<?if (floatval($arResult["DISCOUNT_PRICE_ALL"]) > 0):?>
								<?=$arResult["PRICE_WITHOUT_DISCOUNT"]?>
							<?endif;?>
						</td>
					</tr>

			</table>
			<div style="clear:both;"></div>
		</div>
		<div style="clear:both;"></div>

		<div class="bx_ordercart_order_pay_center">

			<?if ($arParams["USE_PREPAYMENT"] == "Y" && strlen($arResult["PREPAY_BUTTON"]) > 0):?>
				<?=$arResult["PREPAY_BUTTON"]?>
				<span><?=GetMessage("SALE_OR")?></span>
			<?endif;?>

			<a href="javascript:void(0)" onclick="checkOut();" class="checkout"><?=GetMessage("SALE_ORDER")?></a>
		</div>
	</div>
</div>
<?
else:
?>
<div id="basket_items_list">
	<table>
		<tbody>
			<tr>
				<td colspan="<?=$numCells?>" style="text-align:center">
					<h2><?=GetMessage("SALE_NO_ITEMS");?></h2>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?
endif;
?>