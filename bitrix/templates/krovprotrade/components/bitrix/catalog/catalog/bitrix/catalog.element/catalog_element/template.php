<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/card_product_style.css">


<section id="card_product">
	<div class="container-fluide">
		<h2 class="title_section"><?=$arResult['NAME']  ?></h2>
		<div class="row fix">
			<ul class="breadcrumbs">
                <?if(is_array($arResult["TOLEFT"])):?>
                    <li style="float: left"><img src="<?=SITE_TEMPLATE_PATH?>/img/arrow-bread1.png" alt="<?=$arResult['NAME']?>"><a href="<?=$arResult["TOLEFT"]["URL"]?>"><?=$arResult['NAME']?></a></li>
                <?endif?>


                <?if(is_array($arResult["TORIGHT"])):?>
                    <li><a href="<?=$arResult["TORIGHT"]["URL"]?>"><?=$arResult['NAME']?></a><img src="<?=SITE_TEMPLATE_PATH?>/img/arrow-bread.png"></li>
                <?endif?>
            </ul>
		</div>
		<div class="row fix">



			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
		<!-- Start photosgallery-captions -->
				<div class="sliderkit photosgallery-captions">


                    <?if (!empty($arResult['PROPERTIES']['IMAGES']['VALUE'])) {?>
					<div class="sliderkit-nav">
						<div class="sliderkit-nav-clip">
							<ul>
								<?foreach ($arResult['PROPERTIES']['IMAGES']['VALUE'] as $image2):?>
                                    <?php $file = CFile::ResizeImageGet($image2,
                                        array('width'=>105, 'height' => 65), BX_RESIZE_IMAGE_EXACT , true);  ?>
									<li>
                                        <a href="#" rel="nofollow" title="<?=$arResult['NAME']  ?>">
                                            <img src="<?=$image2?>" alt="<?=$arResult['NAME']  ?>" />
                                        </a>
                                    </li>
								<?endforeach;?>
							</ul>
						</div>
						<div class="sliderkit-btn sliderkit-nav-btn sliderkit-nav-prev"><a rel="nofollow" href="#" title="Previous line"><span>Previous line</span></a></div>
						<div class="sliderkit-btn sliderkit-nav-btn sliderkit-nav-next"><a rel="nofollow" href="#" title="Next line"><span>Next line</span></a></div>
						<div class="sliderkit-btn sliderkit-go-btn sliderkit-go-prev"><a rel="nofollow" href="#" title="Previous photo"><span>Previous photo</span></a></div>
						<div class="sliderkit-btn sliderkit-go-btn sliderkit-go-next"><a rel="nofollow" href="#" title="Next photo"><span>Next photo</span></a></div>
					</div>



					<div class="sliderkit-panels">
						<div class="sliderkit-btn sliderkit-go-btn sliderkit-go-prev"><a rel="nofollow" href="#" title="Previous"><span>Previous</span></a></div>
						<div class="sliderkit-btn sliderkit-go-btn sliderkit-go-next"><a rel="nofollow" href="#" title="Next"><span>Next</span></a></div>
                        <?foreach ($arResult['PROPERTIES']['IMAGES']['VALUE'] as $image):?>
                        <?php $file = CFile::ResizeImageGet($image,
                                    array('width'=>500, 'height' => 335), BX_RESIZE_IMAGE_EXACT , true);  ?>
                            <div class="sliderkit-panel">
                                <a href="<?=$image?>" title="<?=$arResult['NAME']  ?>" rel="group1"><img src="<?=$image?>" alt="<?=$arResult['NAME']  ?>" style="width: 490px;"/></a>
                            </div>
                        <?endforeach;?>
					</div>



                    <?} else {?>

                        <div class="sliderkit-panels">
                             <div class="sliderkit-panel">
                                        <img src="/bitrix/templates/krovprotrade/img/no_photo.png" alt="<?=$arResult['NAME']  ?>" style="width: 490px;"/>
                                </div>
                        </div>

                    <?}?>
				</div>
				<div class="spacer"></div>
			</div>


			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding" style="padding-left: 15px;">
				<div class="row" style="padding-left:15px">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
						<form action="#" class="card">
							<p class="form_price">
								<?=number_format($arResult['CATALOG_PRICE_1'], 0, '.', '')?> ���./�2
							</p>
							<p>
								<span>�������:</span><a href="#"> <?=$arResult['PROPERTIES']['ARTICLE']['VALUE'];?></a>
							</p>

							<div class="colors">

                                <?
                                var_dump($arElement["OFFERS"]);

                                foreach($arElement["OFFERS"] as $arOffer):
                                    foreach($arOffer["DISPLAY_PROPERTIES"] as $pid=>$arProperty):
                                        $color .= '\''.$arProperty["DISPLAY_VALUE"].'\',';
                                        break;endforeach;
                                endforeach;
                                ?>



								<?if ($arResult['PROPERTIES']['COLOR']['VALUE']):?>
									<label for="">����:</label>
									<?$i = 1;?>
									<?foreach ($arResult['PROPERTIES']['COLOR']['VALUE'] as $color_value):?>
										<?if ($color_value['DETAIL_PICTURE_SRC']):?>
											<span class="radiobtn">
												<input type="radio" id="radio<?=$i?>" name="prop[COLOR]" value="<?=$color_value?>" <?=$i == 1 ? 'checked' : ''?> >
												<label for="radio<?=$i?>" class="" style="background: url(<?=$color_value['DETAIL_PICTURE_SRC']?>);"></label>
											</span>
											<?$i++;?>
										<?endif;?>
									<?endforeach;?>
								<?endif;?>
							</div>
						</form>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding form_wrap">
                            <form  action="<?=SITE_TEMPLATE_PATH?>/ajax/basket/put_offers_in_basket.php" method="POST" class="buy add_to_cart_form card-purchase">
                                <input type="hidden" name="product_id" value="<?=$arResult["ID"]?>">

                                <?$i = 1;?>
							<?foreach ($arResult['PROPERTIES']['COLOR']['VALUE'] as $color_value):?>
								<input hidden type="radio" data-id="radio<?=$i?>" name="prop[COLOR]" value="<?=$color_value?>" <?=$i == 1 ? 'checked' : ''?> >
								<?$i++;?>
							<?endforeach;?>


							<button name="action" class="add_to_cart">
								<i></i>
								<span>� �������</span>
							</button>

							<input id="input1" type="text" name="quantity" value="1" placeholder="<?=$arResult['CATALOG_QUANTITY'] ? $arResult['CATALOG_QUANTITY'] : 1?>" data-min="1" data-max="<?=$arResult['CATALOG_QUANTITY'] ? $arResult['CATALOG_QUANTITY'] : 1?>" />

							<input type="button" class="plus" rel="nofollow" value="+" />
							<input type="button" class="minus" rel="nofollow" value="-" />

						</form>
					</div>
				</div>

				<?if ($arResult['PROPERTIES']['PROPS']['VALUE']):?>
					<div class="row" style="padding-left:15px">
						<h3>����������� ��������������</h3>
					</div>

					<div class="row" style="padding-left:15px">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
							<ul class="specifications">
								<?foreach ($arResult['PROPERTIES']['PROPS']['VALUE'] as $prop_name):?>
									<li><p><?=$prop_name?></p></li>
								<?endforeach;?>
							</ul>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
							<ul class="values">
								<?foreach ($arResult['PROPERTIES']['PROPS']['DESCRIPTION'] as $prop_value):?>
									<li><p><?=$prop_value?></p></li>
								<?endforeach;?>
							</ul>
						</div>
					</div>
				<?endif;?>
			</div>
		</div>
		<?if ($arResult['DETAIL_TEXT']):?>
			<div class="row fix">
				<h3>�������� ������</h3>
				<p class="description">
					<?=$arResult['DETAIL_TEXT'];?>
				</p>
			</div>
		<?endif;?>
        <?
        //var_dump($arResult);
        $arSelect = Array();
        $arFilter = Array("IBLOCK_ID"=>5, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", 'SECTION_ID' =>$arResult["IBLOCK_SECTION_ID"], "!ID" => $arResult["ID"]);
        $res = CIBlockElement::GetList(Array('RAND' => 'ASC'), $arFilter, false, Array("nPageSize"=>4), $arSelect);
        $total = $res -> SelectedRowsCount();
        if ($total != 0) {
        ?>

		<div class="row fix products">
			<h3>������� ������</h3>

            <?
             while($ob = $res->GetNextElement())
            {
            $arItem = $ob->GetFields();
            $arProps = $ob->GetProperties();
            $photo = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width'=>360, 'height' => 234), BX_RESIZE_IMAGE_EXACT , true);
                ///echo $arFields['NAME'];
            ?>


        <li class="col-lg-3 col-md-3 col-sm-12 col-xs-12 shingles" style="padding: 0; list-style-type: none;">

            <div>

                <?php if($arItem['PREVIEW_PICTURE']){ ?>
                    <?php $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'],
                        array('width'=>230, 'height' => 200), BX_RESIZE_IMAGE_EXACT , true);  ?>
                    <img src="<?=$file["src"]?>" alt="<?=$arItem['NAME']?>">
                <?php }elseif ($arItem['DETAIL_PICTURE']){ ?>
                    <?php $file = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'],
                        array('width'=>230, 'height' => 200), BX_RESIZE_IMAGE_EXACT , true);  ?>
                    <img src="<?=$file["src"]?>" alt="<?=$arItem['NAME']?>">
                <?php }else{ ?>
                    <img src="/bitrix/templates/krovprotrade/img/no_photo.png" alt="<?=$arItem['NAME']?>">
                <?php } ?>


                <?if ($arProps['IS_NEW']['VALUE']):?>
                    <span class="info">
				<i class="fa fa-star" aria-hidden="true"></i>
				<span>�������</span>
			</span>
                <?endif;?>
                <?if ($arProps['OLD_PRICE']['VALUE']):?>
                    <span class="info">
				<i class="fa">-<?=ceil((1 - $arItem['CATALOG_PRICE_1'] / $arProps['OLD_PRICE']['VALUE']) * 100)?> %</i>
				<span class="line-through"><?=number_format($arProps['OLD_PRICE']['VALUE'], 0, '.', '')?> ���.</span>
			</span>
                <?endif;?>


                <?  $price = CCatalogProduct::GetOptimalPrice($arItem["ID"], 1, 'N');
                $arSKU = CCatalogSKU::getOffersList($arItem["ID"], 0, array('ACTIVE' => 'Y'), array('NAME'), array("CODE"=>array('HEIGHT', 'WIDTH', "SIZE")));
                ?>


                <div class="bg_sells">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="title">
                        <?=$arItem['NAME']?>
                    </a>
                    <p class="price">
                        <?if (!empty($arSKU)):?>
                            <?
                            foreach ($arSKU as $key => $value) {
                                $count = 0;
                                foreach ($value as $key2 => $value1) {
                                    $count++;
                                    $price = CCatalogProduct::GetOptimalPrice($value1["ID"], 1, 'N');
                                    if ($count == 1) :
                                        ?>
                                        <?=$price['RESULT_PRICE']["BASE_PRICE"]?> ���.
                                        <?
                                    endif;
                                }
                            }
                            ?>
                        <?else:?>
                            <?=$price['RESULT_PRICE']["BASE_PRICE"]?>
                        <?endif;?> ���.
                    </p>
                </div>
                <div class="shingles-price form_wrap" style="width: 100%;">
                    <form  action="<?=SITE_TEMPLATE_PATH?>/ajax/basket/put_offers_in_basket.php" method="POST" class="add_to_cart_form card-purchase">
                        <input type="hidden" name="product_id" value="<?=$arItem["ID"]?>">

                        <label for="">����:</label>
                        <span class="radiobtn">
					<input type="radio" id="radio1_<?=$ID?>" name="rad" checked="">
					<label for="radio1_<?=$ID?>" class="color-red"></label>
				</span>
                        <span class="radiobtn">
					<input type="radio" id="radio2_<?=$ID?>" name="rad">
					<label for="radio2_<?=$ID?>" class="color-green"></label>
				</span>
                        <span class="radiobtn">
					<input type="radio" id="radio3_<?=$ID?>" name="rad">
					<label for="radio3_<?=$ID?>" class="color-blue"></label>
				</span>
                        <span class="radiobtn">
					<input type="radio" id="radio4_<?=$ID?>" name="rad">
					<label for="radio4_<?=$ID?>" class="color-white"></label>
				</span>
                        <span class="radiobtn">
					<input type="radio" id="radio5_<?=$ID?>" name="rad">
					<label for="radio5_<?=$ID?>" class="color-grey"></label>
				</span>
                        <span class="radiobtn">
					<input type="radio" id="radio6_<?=$ID?>" name="rad">
					<label for="radio6_<?=$ID?>" class="color-beige"></label>
				</span>


                        <!--				<input type="text" name="quantity" value="1" placeholder="--><?//=$arItem['CATALOG_QUANTITY'] ? $arItem['CATALOG_QUANTITY'] : 1?><!--" data-min="1" data-max="--><?//=$arItem['CATALOG_QUANTITY'] ? $arItem['CATALOG_QUANTITY'] : 1?><!--" />-->
                        <!--				<input type="button" class="plus" rel="nofollow" value="+" />-->
                        <!--				<input type="button" class="minus" rel="nofollow" value="-" />-->


                        <div class="number">
                            <span class="minus" style="cursor: pointer">-</span>
                            <input type="text" name="quantity" value="1" size="10"/>
                            <span class="plus" style="cursor: pointer">+</span>
                        </div>

                        <!--				<input type="hidden" name="action" value="ADD2BASKET" />-->
                        <!--				<input type="hidden" name="id" value="--><?//=$arItem['ID']?><!--" />-->

                        <button class="add_to_cart">� �������</button>

                        <p>
                            �������:<a href="#"> <?=$arProps['ARTICLE']['VALUE']?></a>
                        </p>
                    </form>


                </div>

            </div>
        </li>
            <?}?>
		</div>
        <?}?>
	</div>
</section>

<script type="text/javascript">
	$(window).load(function(){ //$(window).load() must be used instead of $(document).ready() because of Webkit compatibility

		// Sliderkit photo gallery > With captions
		$(".photosgallery-captions").sliderkit({
			navscrollatend: true,
			mousewheel:true,
			keyboard:true,
			shownavitems:4,
			auto:false,
			fastchange:true
		});

		// jQuery Lightbox
		var lightboxPath = "/bitrix/templates/krovprotrade/lib/js/external/lightbox/jquery-lightbox/";
		$("a[rel='group1']").lightBox({
			imageLoading:lightboxPath+"images/lightbox-ico-loading.gif",
			imageBtnPrev:lightboxPath+"images/lightbox-btn-prev.gif",
			imageBtnNext:lightboxPath+"images/lightbox-btn-next.gif",
			imageBtnClose:lightboxPath+"images/lightbox-btn-close.gif",
			imageBlank:lightboxPath+"images/lightbox-blank.gif"
		});

	});
</script>