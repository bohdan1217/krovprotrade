<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?
	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
?>

<section id="top_sells">
	<div class="container-fluide">
		<h2 class="title_section"><?=$arResult['NAME']?></h2>
		<?foreach (array_chunk($arResult['ITEMS'], 4) as $arItems):?>
			<div class="row fix products">
				<?foreach ($arItems as $arItem):?>
					<?
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
						$strMainID = $this->GetEditAreaId($arItem['ID']);
						$class = 'col-lg-3 col-md-3 col-sm-4 col-xs-12';
					?>
					<div style="display: inline" id="<?=$strMainID?>" >
						<?=product_render(compact('arItem', 'strMainID', 'class'));?>
						<div class="shingles" style="display: none;"></div>
					</div>
				<?endforeach;?>
			</div>
		<?endforeach;?>
		
		<?=$arResult['NAV_STRING'];?>
	</div>
</section>
