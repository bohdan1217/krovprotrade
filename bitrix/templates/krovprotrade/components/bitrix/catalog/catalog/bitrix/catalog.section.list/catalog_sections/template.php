<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>


<?//var_dump($arResult);?>
<?if ($arResult['SECTIONS']):?>
	<?//var_dump($arResult);?>
		<div class="container-fluide">
          <h1 class="title_section" style="padding-top: 20px;"><?=$arResult['SECTION']['NAME'];?></h1>
			<div class="row fix">
				<div class="otst">
					<div class="row fix catalog-blocks">
						<?foreach ($arResult['SECTIONS'] as $arSection):?>
							<?
							$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
							$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
							?>

                            <?php if($arSection['DETAIL_PICTURE']['SRC']){ ?>
							<div id="<? echo $this->GetEditAreaId($arSection['ID']); ?>" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 products1 block-230" style="padding: 0; background-image: url(<?=$arSection['DETAIL_PICTURE']['SRC'];?>);">
                            <?php }else{ ?>
							<div id="<? echo $this->GetEditAreaId($arSection['ID']); ?>" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 products1 block-230" style="padding: 0; background-image: url(/bitrix/templates/krovprotrade/img/no_photo.png);">
                            <?php } ?>
                                <a href="<? echo $arSection['SECTION_PAGE_URL']; ?>" class="title">
									<? echo $arSection['NAME']; ?>
								</a>
							</div>
						<?endforeach;?>
					</div>
				</div>
			</div>
		</div>
<?else:?>
    <div class="container">
        <p class="catalog-no-products">�������� �� � ������ ������� ���� ��� �������</p>
    </div>
<?endif;?>