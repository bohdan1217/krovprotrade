<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;
//delayed function must return a string
if(empty($arResult) || count($arResult) == 1) {
	return "";
}

$strReturn = '';

$strReturn .= '<section id="breadcrumbs" style="background-color: white !important;"><div class="container-fluide"><div class="row fix"><ul class="breadcrumb breadcrumb-ul">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	$nextRef = ($index < $itemSize-2 && $arResult[$index+1]["LINK"] <> "" ? ' itemref="bx_breadcrumb_'.($index + 1).'"' : '');
	$child = ($index > 0? ' itemprop="child"' : '');
	//$arrow = ($index > 0? '<i class="fa fa-angle-right"></i>' : '');
	$arrow = '';

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize - 1) {
		$strReturn .= '
			<li id="bx_breadcrumb_'.$index.'" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"'.$child.$nextRef.'>
				'.$arrow.'
				<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="url">
					<span itemprop="title">'.$title.'</span>
				</a>
			</li>';
	}
	else
	{
		$strReturn .= '
			<li>
				'.$arrow.'
				<span class="span-breadcrumb">'.$title.'</span>
			</li>';
	}
}

$strReturn .= '</ul></div></div></section>';

return $strReturn;
