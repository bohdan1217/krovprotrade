<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<!-- works -->
<section id="works">
	<div class="container-fluide">
		<div class="row fix">
			<h2 class="title_section">���� ������</h2>
			
				<div class="row fix work-blocks">
					<?foreach($arResult["ITEMS"] as $arItem):?>
                        <?php if($arItem['PREVIEW_PICTURE']){ ?>
                            <?php $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'],
                                array('width'=>230, 'height' => 200), BX_RESIZE_IMAGE_EXACT , true);  ?>
                            <div class="block-work block-230 img-1" style="background-image: url(<?=$file["src"]?>);">
                        <?php }else{ ?>
                                <div class="block-work block-230 img-1" style="background-image: url(/bitrix/templates/krovprotrade/img/no_photo.png);">
                        <?php } ?>
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                <?php if($arItem['PREVIEW_TEXT']){ ?>
                                    <p class="work-text"><?=$arItem['PREVIEW_TEXT']?></p>
                                <?php }else{ ?>
                                    <p class="work-text"><?=$arItem['NAME']?></p>
                                <?php } ?>
                            </a>
						</div>
					<?endforeach;?>
				</div>
			
<!--				<div class="row fix ">-->
<!--					<ul class="works-slider">-->
<!--						--><?//foreach($arResult["ITEMS"] as $arItem):?>
<!--							<li class="item">-->
<!--								<div class="block-work block-230 img-1" style="--><?//=$arItem['DETAIL_PICTURE']['SRC']?><!--">-->
<!--									<p class="work-text">���������� �������, ������� �������, <br> ������ ����� �� �����</p>-->
<!--								</div>-->
<!--							</li>-->
<!--						--><?//endforeach;?>
<!--					</ul>-->
<!--				</div>-->
			
			<div class="row fix">
				<a href="/our_works/">
					<button>
						������� �� ���� �������
					</button>
				</a>
			</div>
			
		</div>
	</div>
</section>

<?/*
<section id="works">
	<div class="container-fluide">
		<div class="row fix">
			<h2 class="title_section">���� ������</h2>
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12  block-230 img-1" style="<?=$arItem['DETAIL_PICTURE']['SRC']?>">
					<div class="work-text">
						<p><?=$arItem['~PREVIEW_TEXT']?></p>
					</div>
				</div>
			<?endforeach;?>
			<div class="row fix">
				<a href="/our_works/">
					<button>
						������� �� ���� �������
					</button>
				</a>
			</div>
			
		</div>
	</div>
</section>*/?>

