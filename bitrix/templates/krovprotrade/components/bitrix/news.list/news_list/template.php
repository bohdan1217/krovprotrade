<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalCss("/bitrix/css/main/bootstrap.css");
$this->addExternalCss("/bitrix/css/main/font-awesome.css");
$this->addExternalCss($this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css');
?>

<section id="news" class="news-background">
	<div class="container-fluide">
		<h2 class="title_section">�������</h2>
		<div class="row fix equalheightnews">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	
	<div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="col-lg-4 col-md-4 col-sm-6 col-xs-12 no-padding2">
		<div class="item-news">
			<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="" style="width: 295px;">

                <div style="background-color: #F0F0F0; margin-bottom: 20px">
                    <div style="margin-top: -12px; margin-bottom: 15px;">
                        <span class="info" style="position: relative !important;">
                            <span ><?=$arItem['TIMESTAMP_X']?></span>
                        </span>
                    </div>
                    <div class="desc" style="padding: 15px;">
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="title" style="font-size: 16px;">
                            <?=$arItem['NAME']?>
                        </a><br>
                        <div class="op" style="padding-top: 10px;">
                            <?=$arItem['PREVIEW_TEXT']?>
                        </div>
                    </div>
            </div>
		</div>
	</div>
	
<?endforeach;?>
			</div>
		</div>
	</div>
</section>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

