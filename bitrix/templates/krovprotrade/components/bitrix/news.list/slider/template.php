<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section id="slider">
    <div class="container-fluide">
        <div class="row fix1 slider-main">
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 slider-bg no-padding">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                    <?foreach($arResult["ITEMS"] as $key1 => $arItem1):?>
                        <li data-target="#myCarousel" data-slide-to="<?=$key1?>"  <?if ($key1 == 0) :?>class="active"<?endif;?>></li>
                    <?endforeach;?>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?foreach($arResult["ITEMS"] as $key => $arItem):?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                            <div class="item  <?if ($key == 0) :?>active<?endif;?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                <p>
                                    <?=$arItem['PROPERTIES']['DESC']["VALUE"]['TEXT']?>
                                </p>
                                <?if (!empty($arItem['PROPERTIES']['LINK']["VALUE"])) {?>
                                <button>
                                    <a href=" <?=$arItem['PROPERTIES']['LINK']["VALUE"]?>" class="href-slider">
                                        подробнее
                                    </a>
                                </button>
                                <?}?>
                            </div>
                        <?endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>