<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_MAIN"), $arResult['SEF_FOLDER']);
$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_SUBSCRIBE_NEW"));
/*$APPLICATION->IncludeComponent(
	'bitrix:catalog.product.subscribe.list',
	'',
	array('SET_TITLE' => $arParams['SET_TITLE_SUBSCRIBE'])
	,
	$component
);*/

$APPLICATION->IncludeComponent("bitrix:subscribe.index", "subscribe_news", Array(
	"SHOW_COUNT" => "Y",	// ���������� ���������� �����������
		"SHOW_HIDDEN" => "Y",	// �������� ������� ������� ��������
		"PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",	// �������� �������������� �������� (�������� ������ #SITE_DIR#)
		"CACHE_TYPE" => "A",	// ��� �����������
		"CACHE_TIME" => "3600",	// ����� ����������� (���.)
		"SET_TITLE" => "Y",	// ������������� ��������� ��������
	),
	false
);?>