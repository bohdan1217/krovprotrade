<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;

?>

<section id="user">
	<div class="container-fluide">
		<div class="row fix">
			<h2 class="title_section">������� ������������</h2>
			
			<?
				ShowError($arResult["strProfileError"]);
				if ($arResult['DATA_SAVED'] == 'Y') {
					ShowNote(Loc::getMessage('PROFILE_DATA_SAVED'));
				}
			?>
			
			<form method="post" action="<?=$APPLICATION->GetCurUri()?>" enctype="multipart/form-data" role="form">
				<?=$arResult["BX_SESSION_CHECK"]?>
				<input type="hidden" name="lang" value="<?=LANG?>" />
				<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
				<input type="hidden" name="LOGIN" value=<?=$arResult["arUser"]["LOGIN"]?> />
			
				<h3 class="title_form">������ ������</h3>
					<div class="row main-form">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
							<input type="text" name="NAME" placeholder="���� ���" value="<?=$arResult["arUser"]["NAME"]?>" />
						</div>
						
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding form-content">
								<input type="text" name="EMAIL" value="<?=$arResult["arUser"]["EMAIL"]?>" placeholder="Email" />
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding form-content">
								<input type="text" name="PERSONAL_PHONE" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" placeholder="�������" />
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding form-content">
								<input type="text" name="NEW_PASSWORD" placeholder="����� ������" />
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding form-content">
								<input type="text" name="NEW_PASSWORD_CONFIRM" placeholder="������������� ������" />
							</div>
						</div>
						<div class="row social-content">
                            <p>
                                ��������� ���������� ����:
                            </p>
                            <p class="socials">
                                <a href="#"><i class="fa fa-lg fa-vk" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-lg fa-twitter" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-lg fa-facebook" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-lg fa-instagram" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-lg fa-google" aria-hidden="true"></i></a>
                            </p>
                        </div>
						<div class="row">
							<button name="save" value="���������">
								��������� ���������
							</button>
						</div>
					</div>
			</form>
		</div>
	</div>
	
</section>
