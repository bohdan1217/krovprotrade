<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<section id="menu">
	<div class="container-fluide">
		<div class="row fix1">
			<nav class="navbar navbar-default">
				<div class="navbar-header">
					  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">����</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
					  <div class="time-menu">
						<img src="<?=SITE_TEMPLATE_PATH?>/img/imgpsh_fullsize (1).png" alt="time">
						<div class="time-text_adaptiv">
							<a href="tel:8 (495) 646-75-10" class="tellto">8 (495) 646-75-10</a>
							<p>�� � �� � 9:00 �� 18:00</p>
							<a href="mailto:8829945@mail.ru" class="info-upper">8829945@mail.ru</a>
						</div>
					</div>
				</div>
<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<?foreach($arResult as $arItem):?>
							<li class="<?=$arItem["SELECTED"] ? 'active' : '';?>"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
								<?if ($arItem['TREE']):?>
									<div class="big_submenu_wrap">
										<div class="big_submenu">
											<?foreach ($arItem['TREE'] as $arItem_1):?>
												<div class="submenu">
													<a class="level1_submenu_item" href="<?=$arItem_1['SECTION_PAGE_URL']?>"><?=$arItem_1['NAME']?></a>
													<div class="subment_right_content_wrap">
														<div class="subment_right_content">
															<?foreach ($arItem_1['TREE'] as $arItem_2):?>
																<a class="level2_submenu_item" href="<?=$arItem_2['SECTION_PAGE_URL']?>"><?=$arItem_2['NAME']?></a>
																<?foreach ($arItem_2['TREE'] as $arItem_3):?>
																	<a class="level3_submenu_item" href="<?=$arItem_3['SECTION_PAGE_URL']?>"><?=$arItem_3['NAME']?></a>
																<?endforeach;?>
															<?endforeach;?>
														</div>
													</div>
												</div>
											<?endforeach;?>
										</div>
									</div>
								<?endif;?>
							</li>
						<?endforeach;?>
					</ul>
				</div><!-- /.navbar-collapse -->
			</nav>
		</div>
	</div>
</section>

<?/*
<section id="menu">
	<div class="container-fluide">
		<div class="row fix1">
			<nav class="navbar navbar-default">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav">
					<?foreach($arResult as $arItem):?>
						<li class="<?=$arItem["SELECTED"] ? 'active' : '';?>"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
							<?if ($arItem['TREE']):?>
								<div class="big_submenu_wrap">
									<div class="big_submenu">
										<?foreach ($arItem['TREE'] as $arItem_1):?>
											<div class="submenu">
												<a class="level1_submenu_item" href="<?=$arItem_1['SECTION_PAGE_URL']?>"><?=$arItem_1['NAME']?></a>
												<div class="subment_right_content_wrap">
													<div class="subment_right_content">
														<?foreach ($arItem_1['TREE'] as $arItem_2):?>
															<a class="level2_submenu_item" href="<?=$arItem_2['SECTION_PAGE_URL']?>"><?=$arItem_2['NAME']?></a>
															<?foreach ($arItem_2['TREE'] as $arItem_3):?>
																<a class="level3_submenu_item" href="<?=$arItem_3['SECTION_PAGE_URL']?>"><?=$arItem_3['NAME']?></a>
															<?endforeach;?>
														<?endforeach;?>
													</div>
												</div>
											</div>
										<?endforeach;?>
									</div>
								</div>
							<?endif;?>
						</li>
					<?endforeach;?>
				  </ul>
				</div><!-- /.navbar-collapse -->
			</nav>
		</div>
	</div>
</section>
*/?>

<script>
	var hack_visible = { position: 'absolute', visibility: 'hidden', display: 'block' };
	var hack_invisible = { position: '', visibility: '', display: '' };
	$('.subment_right_content').each(function () {
		var block_height = 0.0;
		var parent = $(this);
		$(this).children().each(function () {
			var elem = $(this);
			
			elem.css(hack_visible);
			parent.css(hack_visible);
			parent.parent().css(hack_visible);
			$('.big_submenu_wrap').css(hack_visible);
			
			block_height += elem.outerHeight(true) + 1;
			
			elem.css(hack_invisible);
			parent.css(hack_invisible);
			parent.parent().css(hack_invisible);
			$('.big_submenu_wrap').css(hack_invisible);
		});
		parent.css({height: Math.ceil(block_height / 2.0) + 100});
	});
</script>