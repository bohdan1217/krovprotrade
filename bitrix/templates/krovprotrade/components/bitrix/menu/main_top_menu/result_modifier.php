<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
	function set_by_path(&$array, $path, $value) {
		$temp = &$array;
		foreach ($path as $key) {
			$temp = &$temp[$key];
		}
		$temp = $value;
		unset($temp);
	}
	
	$db_sections = CIBlockSection::GetTreeList(
		array(
			'IBLOCK_ID' => STUFF_IBLOCK_ID,
		),
		array('ID', 'NAME', 'SECTION_PAGE_URL', 'DEPTH_LEVEL')
	);
	
	$path = array();
	$depth = 0;
	while ($ar_section = $db_sections->GetNext()) {
		if ($ar_section['DEPTH_LEVEL'] > $depth) {
			array_push($path, 'TREE', $ar_section['ID']);
		} else if ($ar_section['DEPTH_LEVEL'] < $depth) {
			foreach (range(1, $depth - $ar_section['DEPTH_LEVEL']) as $value) {
				array_pop($path);
				array_pop($path);
			}
		}
		
		array_pop($path);
		array_pop($path);
		
		array_push($path, 'TREE', $ar_section['ID']);
		
		$depth = $ar_section['DEPTH_LEVEL'];
		set_by_path($arResult[0], $path, $ar_section);
	}
	
	