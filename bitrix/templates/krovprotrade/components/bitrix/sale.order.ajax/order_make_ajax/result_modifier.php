<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

if (isset($_REQUEST['BasketOrder'])) {
	$arFields = array(
		"SITE_ID" => "s1",
		//"PAYED" => "N",
		//"CANCELED" => "N",
		//"STATUS_ID" => "N",
		"CURRENCY" => "RUB",
		"USER_ID" => IntVal($USER->GetID()),
		/*"PAY_SYSTEM_ID" => 3,
		"PRICE_DELIVERY" => 11.37,
		"DELIVERY_ID" => 2,
		"DISCOUNT_VALUE" => 1.5,
		"TAX_VALUE" => 0.0,
		"USER_DESCRIPTION" => ""*/
	);
	
	$arBasketItems = array();
	$dbBasketItems = CSaleBasket::GetList(
        array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
        array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL"
            ),
        false,
        false,
        array("ID", "CALLBACK_FUNC", "MODULE", 
              "PRODUCT_ID", "QUANTITY", "DELAY", 
              "CAN_BUY", "PRICE", "WEIGHT")
    );
	$allSum = 0;
	while ($ar_item = $dbBasketItems->GetNext()) {
		$allSum += $ar_item['PRICE'] * $ar_item['QUANTITY'];
		$arBasketItems[] = $ar_item;
	}
	$arFields['PRICE'] = $allSum;

	if (isset($_REQUEST['DELIVERY_ID'])) {
		$arFields['DELIVERY_ID'] = $_REQUEST['DELIVERY_ID'];
		if ($arFields['DELIVERY_ID'] == 2) {
			$arFields['PRICE_DELIVERY'] = 600;
		} else {
			$arFields['PRICE_DELIVERY'] = 0;
		}
		
		$arFields['PRICE'] += $arFields['PRICE_DELIVERY'];
	}
	
	if (isset($_REQUEST['PAY_SYSTEM_ID'])) {
		$arFields['PAY_SYSTEM_ID'] = $_REQUEST['PAY_SYSTEM_ID'];
	}
	
	if (isset($_REQUEST['PERSON_TYPE_ID'])) {
		$arFields['PERSON_TYPE_ID'] = $_REQUEST['PERSON_TYPE_ID'];
	}
	$order_id = CSaleOrder::Add($arFields);

	if ($order_id) {
		LocalRedirect('?ORDER_ID='.$order_id);
	}
}
 
$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);