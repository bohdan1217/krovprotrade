<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var $APPLICATION CMain
 */

if ($arParams["SET_TITLE"] == "Y")
	$APPLICATION->SetTitle(Loc::getMessage("SOA_ORDER_COMPLETE"));
?>

<?CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());?>

<?$arPaySystem = $arResult['PAY_SYSTEM_LIST'][array_slice($arResult["PAYMENT"], 0, 1)[0]["PAY_SYSTEM_ID"]];?>

<section id="ordering">
	<div class="container-fluide">
		<div class="row fix">
			<h2>���������� ������</h2>
			<p><b>��� �����:</b> �<?=$arResult['ORDER']['ID']?> �� <?=$arResult['ORDER']['DATE_INSERT']->format("d.m.Y")?> ������� ������</p>
			<p>�� ������ ������� �� ����������� ������ ������ � <a href="/personal/">������������ ������� �����</a><br>
			�������� ��������, ��� ��� ����� � ���� ������ ��� ���������� �����  ������ ����� � ������ ������������ �����.</p>
			<p><b>������ ��������:</b> <?=$arResult['ORDER']['DELIVERY_ID'] == 2 ? '�������� ��������' : '���������'?></p>
			<p><b>������ ������:</b> <?=array_slice($arResult['PAYMENT'], 0, 1)[0]['PAY_SYSTEM_NAME']?></p>
			<p><b>����� � ������:</b> <?=number_format($arResult['ORDER']['PRICE'], 0, '.', '')?> ���.</p>
			
			<div style="display: none">
				<?=$arPaySystem["BUFFERED_OUTPUT"]?>
			</div>
			
			<button class="button-dark" onclick="$('[name=BuyButton]').click(); return false;" >
					��������
			</button>
		</div>
	</div>
	
</section>
