<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
CJSCore::Init(array('clipboard'));

Loc::loadMessages(__FILE__);

?>

<section id="my_order">
	<div class="container-fluide">
		<div class="row fix">
			<h2>��� ������</h2>

			<div class="row fix">
				<ul class="pages clearfix">
					<li><a href="/personal/orders/?show_all=Y" class="<?=$_SERVER['REQUEST_URI'] == '/personal/orders/?show_all=Y' ? 'active' : '' ?>" >��� ������</a></li>
					<li><a href="/personal/orders/?filter_history=Y" class="<?=$_SERVER['REQUEST_URI'] == '/personal/orders/?filter_history=Y' ? 'active' : '' ?>">������� �������</a></li>
					<li><a href="/personal/orders/" class="<?=$_SERVER['REQUEST_URI'] == '/personal/orders/' ? 'active' : '' ?>">������� ������</a></li>
				</ul>	
			</div>
			<?foreach ($arResult['ORDERS'] as $order):?>
				<div class="form">
					<div class="row fix form-header">
						<h3>����� <?=$order['ORDER']['ID']?> �� <?=$order['ORDER']['DATE_INSERT']->format("d.m.Y")?></h3>
                        <a href="<?=$order['ORDER']["URL_TO_DETAIL"]?>">��������� � ������</a>
					</div>
					<div class="row fix form-content" style="background-color: white;">
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 no-padding">
							<p><b>����� � ������:</b> <?=number_format($order['ORDER']['PRICE'], 0, '.', ' ')?> ���.</p>
                            <p><b>��������� ������:</b> <?=$order["SHIPMENT"][0]["DELIVERY_STATUS_NAME"]?> </p>
							<p><b>������ ������:</b> <?=$order["PAYMENT"][0]['PAY_SYSTEM_NAME']?></p>
							<p><b>������ ��������:</b><?=$order["SHIPMENT"][0]["DELIVERY_NAME"]?></p>
<!--							<p><b>������ ������:</b></p>-->
							<?$i = 1;?>
							<?foreach ($order['BASKET_ITEMS'] as $stuff):?>
								<p><?=$i?>. <a href="<?=$stuff['DETAIL_PAGE_URL']?>" ><?=$stuff['NAME']?></a></p>
								<?$i++;?>
							<?endforeach;?>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 no-padding">
						<time><?=$order['ORDER']['DATE_INSERT']->format("d.m.Y")?></time>
						<input type="text" placeholder="������, ��������� ������ ">
						
						<a href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_CANCEL"])?>" style="text-decoration: none !important;" >
							<button class="button-dark cancel">
								�������� �����
							</button>
						</a>
						<a href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"])?>" style="text-decoration: none !important;" >
							<button class="bg-blue repeat">
								��������� �����
							</button>
						</a>
						</div>
					</div>
				</div>
			<?endforeach;?>

	<?=$arResult['NAV_STRING']?>
	
</section>