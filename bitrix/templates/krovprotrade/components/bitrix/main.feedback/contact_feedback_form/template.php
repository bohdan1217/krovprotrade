<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<form class="contact-form" action="<?=POST_FORM_ACTION_URI?>" method="POST" >
	<?=bitrix_sessid_post()?>
	<h2 class="title_form"> �������� ����� </h2>
		<div class="row main-form">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="form-content">
					<input type="text" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>" placeholder="���� ���" />
				</div>
				<div class="form-content">
					<input type="text" name="AUTHOR_PHONE" value="<?=$arResult["AUTHOR_PHONE"]?>" placeholder="���������� �������" />
				</div>
				<div class="form-content">
					<input type="email" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>" placeholder="E-mail" />
				</div>
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="form-content">
					<textarea name="MESSAGE" value="<?=$arResult["MESSAGE"]?>" placeholder="���� ���������" ></textarea>
				</div>
			</div>
			
			<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>" />
			<div class="row">
				<button name="submit" value="<?=GetMessage("MFT_SUBMIT")?>" type="submit">
					���������
				</button>
			</div>
		</div>
</form>
