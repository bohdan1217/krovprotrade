<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<!-- catalog -->

		<h2 class="title_section">Каталог продукции</h2>
			<div class="row fix">
				<div class="row fix catalog-blocks">
					<?foreach ($arResult['SECTIONS'] as $arSection):?>
						<?
							$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
							$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
						?>
						<div id="<? echo $this->GetEditAreaId($arSection['ID']); ?>" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 products1 block-230" style="padding: 0; background-image: url(<?=$arSection['DETAIL_PICTURE']['SRC'];?>">
							<a href="<?=$arSection['SECTION_PAGE_URL']?>" class="title">
								<?=$arSection['NAME']?>
							</a>
						</div>
					<?endforeach;?>
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 products8 block-230" style="padding: 0">
					</div>
				</div>
<!-- products adaptiv max-width 991px -->
<!--				<ul class="catalog-slider">-->
<!--					--><?//foreach ($arResult['SECTIONS'] as $arSection):?>
<!--						--><?//
//							$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
//							$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
//						?>
<!--						<li class="item">-->
<!--							<div class="col-lg-3 col-md-3 col-sm-12 products1 block-230" style="padding: 0; background-image: url(--><?//=$arSection['DETAIL_PICTURE']['SRC'];?><!--">-->
<!--								<a href="--><?//=$arSection['SECTION_PAGE_URL']?><!--" class="title">--><?//=$arSection['NAME']?><!--</a>-->
<!--							</div>-->
<!--						</li>-->
<!--					--><?//endforeach?>
<!--				</ul>-->
<!-- products adaptiv max-width 991px -->
				</div>



