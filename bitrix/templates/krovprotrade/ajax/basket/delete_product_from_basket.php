<?php
	$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
	require($DOCUMENT_ROOT. "/bitrix/modules/main/include/prolog_before.php");

	define("NO_KEEP_STATISTIC", true); // Не собираем стату по действиям AJAX

	CModule::IncludeModule('catalog');
	CModule::IncludeModule("sale");

	$product = $_REQUEST['product'];
	
	$basket_product = CSaleBasket::GetList(
	 array("ID" => "DESC"),
	 array(
	  "PRODUCT_ID" => $product,
	  "FUSER_ID" => CSaleBasket::GetBasketUserID(),
	  "LID" => SITE_ID,
	  "ORDER_ID" => "NULL"
	 ),
	 false,
	 false,
	 array()
	)->Fetch();

	// Update product quantity
	
	if ($basket_product)
	{
		CSaleBasket::Delete($basket_product['ID']);
		if(isset($_REQUEST['ajax']))
			echo 1;
		else
			LocalRedirect($_SERVER['HTTP_REFERER']);
	}

	return; 

