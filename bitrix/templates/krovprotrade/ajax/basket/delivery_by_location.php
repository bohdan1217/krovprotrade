<?php
	$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
	require($DOCUMENT_ROOT. "/bitrix/modules/main/include/prolog_before.php");

	define("NO_KEEP_STATISTIC", true); // Не собираем стату по действиям AJAX

	CModule::IncludeModule('catalog');
	CModule::IncludeModule("sale");

  $location = intval($_GET['location']);
  $price = $_GET['price'];
  $weight = $_GET['weight'];


  $db_dtype = CSaleDelivery::GetList(
      array(
              "SORT" => "ASC",
              "NAME" => "ASC"
          ),
      array(
              "LID" => SITE_ID,
              "+<=WEIGHT_FROM" => $weight,
              "+>=WEIGHT_TO" => $weight,
              "+<=ORDER_PRICE_FROM" => $price,
              "+>=ORDER_PRICE_TO" => $price,
              "ACTIVE" => "Y",
              "LOCATION" => $location
          ),
      false,
      false,
      array()
  );

  if ($ar_dtype = $db_dtype->Fetch())
  {
    $icons_arr = array(
      'blue',
      'green',
      'red',
    );
        $counter = 0; 
     do
     {
      $checked = '';
      if($counter == 0){
        $first_delivery = $ar_dtype['ID'];
        $checked = ' checked';
        $delivery_price = $ar_dtype['PRICE'];
      }

      $descriptuion = '';
      if(!empty($ar_dtype['DESCRIPTION']))
        $descriptuion = '<span>'. $ar_dtype['DESCRIPTION'] . '</span>';

      $delivery_html .= '<li class="c-radio-list">
                  <label>
                    <input 
                      type="radio" 
                      data-price="'. $ar_dtype['PRICE'] .'"
                      value="'. $ar_dtype['ID'] .'"
                      '. $checked .'
                      name="DELIVERY_ID"
                    >
                    <div class="list-cnt">
                      <div class="ttl">
                        <div class="cnt">
                          <div class="payment__item">
                            <i class="ico ico-delivery-'. $icons_arr[$counter] .'"></i>
                            <span>'. $ar_dtype['NAME'] .'</span>
                          </div>
                        </div>
                        <div class="price"><font>'. $ar_dtype['PRICE'] .'</font>р.</div>
                      </div>
                      '. $descriptuion .'
                    </div>
                  </label>
                </li>';
      
      $counter++; 
     }
     while ($ar_dtype = $db_dtype->Fetch());
     echo $delivery_html;
  }