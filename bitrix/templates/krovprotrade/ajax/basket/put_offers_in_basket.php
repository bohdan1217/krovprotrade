<?php
	$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
	require($DOCUMENT_ROOT. "/bitrix/modules/main/include/prolog_before.php");

	define("NO_KEEP_STATISTIC", true); // Не собираем стату по действиям AJAX


	$products_arr = $_POST;
	$quantity_arr = $_POST;
	CModule::IncludeModule("sale");
	if(is_array($_POST['product_id'])){

		foreach($_POST['product_id'] as $key => $product_id){
			$item_id = sw_add_product_to_basket($product_id, $_POST['quantity'][$key]);

			CSaleBasket::Update($item_id, array(
              "DELAY" => "N"
            ));
		}
	}else{
		$quantity = 1;
		if(!empty($_POST['quantity']) && $_POST['quantity'] > 0)
			$quantity = $_POST['quantity'];
		$item_id = sw_add_product_to_basket($_POST['product_id'], $quantity); 

		CSaleBasket::Update($item_id, array(
          "DELAY" => "N"
        ));
	}

	if(cur_page != '/cart/'){
    ?>
<!--		header_cart-->

        <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "", Array(
                "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N", // Рассчитывать скидку для каждой позиции (на все количество товара)
                "COLUMNS_LIST" => array(    // Выводимые колонки
                    0 => "NAME",
                    1 => "DISCOUNT",
                    2 => "PRICE",
                    3 => "QUANTITY",
                    4 => "SUM",
                    5 => "PROPS",
                    6 => "DELETE",
                    7 => "DELAY",
                    8 => "PREVIEW_TEXT",
                    9 => "PROPERTY_VENDOR_CODE",
                    10 => 'PROPERTY_PACKAGE',
                    11 => 'PROPERTY_CML2_LINK'
                ),
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "PATH_TO_ORDER" => "/personal/order/make/", // Страница оформления заказа
                "HIDE_COUPON" => "N",   // Спрятать поле ввода купона
                "QUANTITY_FLOAT" => "N",    // Использовать дробное значение количества
                "PRICE_VAT_SHOW_VALUE" => "Y",  // Отображать значение НДС
                "SET_TITLE" => "Y", // Устанавливать заголовок страницы
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );?>
    <?php }
