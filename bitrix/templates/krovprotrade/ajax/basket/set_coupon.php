<?php
	$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
	require($DOCUMENT_ROOT. "/bitrix/modules/main/include/prolog_before.php");

	define("NO_KEEP_STATISTIC", true); // Не собираем стату по действиям AJAX

	CModule::IncludeModule('catalog');
	CModule::IncludeModule("sale");

	$coupon = $_POST['coupon'];

	if(CCatalogDiscount::SetCoupon($coupon))
		echo 1;
	else
		echo 'Не верный код купона';

	return;


