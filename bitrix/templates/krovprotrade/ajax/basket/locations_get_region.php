<?php
	$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
	require($DOCUMENT_ROOT. "/bitrix/modules/main/include/prolog_before.php");

	define("NO_KEEP_STATISTIC", true); // Не собираем стату по действиям AJAX

	CModule::IncludeModule('catalog');
	CModule::IncludeModule("sale");

      $country = $_POST['country'];


   $db_vars = CSaleLocation::GetList(
        array(
                "SORT" => "ASC",
                "COUNTRY_NAME_LANG" => "ASC",
                "REGION_NAME_LANG" => "ASC"
            ),
        array("LID" => LANGUAGE_ID, 'COUNTRY_ID' => $country, 'CITY_ID' => false),
        false,
        false,
        array()
    );
   while ($vars = $db_vars->Fetch()):

      ?>
      <option value="<?= $vars["ID"]?>"><?=htmlspecialchars($vars["REGION_NAME"])?></option>
      <?
   endwhile;
   