<?php
	$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
	require($DOCUMENT_ROOT. "/bitrix/modules/main/include/prolog_before.php");

	define("NO_KEEP_STATISTIC", true); // Не собираем стату по действиям AJAX

	CModule::IncludeModule('catalog');
	CModule::IncludeModule("sale");

	$item = (int) $_POST['item'];
	$quantity = (int) $_POST['quantity'];

	CSaleBasket::Update($item, array(
	  "QUANTITY" => $quantity
	));

	$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "sale.line", Array(
                  "PATH_TO_BASKET" => "/personal/cart/",  // Страница корзины
                  "PATH_TO_PERSONAL" => "/personal/", // Страница персонального раздела
                  "SHOW_PERSONAL_LINK" => "Y",  // Отображать персональный раздел
                  "SHOW_NUM_PRODUCTS" => "Y", // Показывать количество товаров
                  "SHOW_TOTAL_PRICE" => "Y",  // Показывать общую сумму по товарам
                  "SHOW_EMPTY_VALUES" => "Y", // Выводить нулевые значения в пустой корзине
                  "SHOW_PRODUCTS" => "Y", // Показывать список товаров
                  "POSITION_FIXED" => "Y",  // Отображать корзину поверх шаблона
                  "POSITION_HORIZONTAL" => "right", // Положение по горизонтали
                  "POSITION_VERTICAL" => "top", // Положение по вертикали
                  "PATH_TO_ORDER" => SITE_DIR."personal/order/make/",  // Страница оформления заказа
                  "SHOW_DELAY" => "Y",  // Показывать отложенные товары
                  "SHOW_NOTAVAIL" => "Y", // Показывать товары, недоступные для покупки
                  "SHOW_SUBSCRIBE" => "Y",  // Показывать товары, на которые подписан покупатель
                  "SHOW_IMAGE" => "Y",  // Выводить картинку товара
                  "SHOW_PRICE" => "Y",  // Выводить цену товара
                  "SHOW_SUMMARY" => "Y",  // Выводить подытог по строке
                ),
                false
            );

	return; 

