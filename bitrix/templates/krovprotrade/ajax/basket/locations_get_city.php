<?php
	$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
	require($DOCUMENT_ROOT. "/bitrix/modules/main/include/prolog_before.php");

	define("NO_KEEP_STATISTIC", true); // Не собираем стату по действиям AJAX

	CModule::IncludeModule('catalog');
	CModule::IncludeModule("sale");

  $country = 1;
  if(!empty($_POST['country']))
    $country = $_POST['country'];
  $region = $_POST['region'];


   $db_vars = CSaleLocation::GetList(
        array(
                "SORT" => "ASC",
                "COUNTRY_NAME_LANG" => "ASC",
                "CITY_NAME_LANG" => "ASC"
            ),
        array("LID" => LANGUAGE_ID, 'COUNTRY_ID' => $country, 'REGION_ID' => $region),
        false,
        false,
        array()
    );
   while ($vars = $db_vars->Fetch()):
      ?>
      <option value="<?= $vars["ID"]?>"><?=htmlspecialchars($vars["CITY_NAME"])?></option>
      <?
   endwhile;
   