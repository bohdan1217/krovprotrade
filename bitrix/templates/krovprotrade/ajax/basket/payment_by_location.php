<?php
	$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
	require($DOCUMENT_ROOT. "/bitrix/modules/main/include/prolog_before.php");

	define("NO_KEEP_STATISTIC", true); // Не собираем стату по действиям AJAX

	CModule::IncludeModule('catalog');
	CModule::IncludeModule("sale");

  $first_delivery = intval($_GET['delivery']);

  $pay_arr = array(
    'gray',
    'yandex',
    'visa'
  );

  $db_ptype = CSalePaySystem::GetList(
    $arOrder = Array("SORT"=>"ASC"),
    Array(
      "LID"=>SITE_ID, 
      "CURRENCY"=>"RUB", 
      "ACTIVE"=>"Y", 
      "PERSON_TYPE_ID" => 1
    )
  );

  $result_items = '';

      $counter = 0;
      while ($ptype = $db_ptype->Fetch())
      {
        $avaliable = sw_check_payment_avaliable($ptype['ID'], $first_delivery); 
        if(!$avaliable)
          continue;

        $checked = '';
        if($counter == 0)
          $checked = ' checked';
        $result_items .= '
        <label>
                <input type="radio" name="PAY_SYSTEM_ID"'. $checked .' value="'. $ptype["ID"] .'" >
                <div class="list-cnt">
                  <div class="payment__item">
                    <i class="ico ico-'. $pay_arr[$counter] .'-kosh"></i>
                    <span>'. $ptype['NAME'] .'</span>
                  </div>
                </div>
              </label>
              ';

        $counter++; 
      } 
      echo $result_items;