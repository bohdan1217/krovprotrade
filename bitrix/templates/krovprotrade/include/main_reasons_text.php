<section id="reasons">
	<div class="container-fluide">
		<div class="row fix">
			<h2 class="title_section"> 10 ������ ���������� � ���</h2>
			<div class="row fix reason-blocks">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 reason-block">
					<div class="reason-number">
						1
					</div>
					<p class="reason-content-small">
						���������� ����� ��������� �� ������
					</p>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 reason-block">
					<div class="reason-number">
						2
					</div>
					<p>
						�������� �������������� ������ �� ���� ����������, �������������� <br> � ��������
					</p>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 reason-block">
					<div class="reason-number">
						3
					</div>
					<p>
						������� �������� ���������� ���������� � ������ ���������� �� ������
					</p>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 reason-block">
					<div class="reason-number">
						4
					</div>
					<p class="reason-content-small">
						�������� ���������� � ����� ����� ��
					</p>
				</div>
			</div>

			<div class="row fix reason-blocks">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 reason-block">
					<div class="reason-number">
						5
					</div>
					<p>
						������� �������� ���������� ���������� � ������ ���������� �� ������
					</p>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 reason-img">
					<img src="<?=SITE_TEMPLATE_PATH?>/img/roof.png" alt="roof">
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 reason-img img-roof" style="padding-right: 50px;">
					<img src="<?=SITE_TEMPLATE_PATH?>/img/roof2.png" alt="roof">
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 reason-block">
					<div class="reason-number">
						6
					</div>
					<p class="reason-content-small">
						���������� ����� ��������� �� ������
					</p>
				</div>
			</div>

			<div class="row fix reason-blocks">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 reason-block">
					<div class="reason-number">
						7
					</div>
					<p class="reason-content">
						����� 10 ��� �� ����� ���������� ���������� � �� �������
					</p>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 reason-block">
					<div class="reason-number">
						8
					</div>
					<p class="reason-content">
						���������������� ���� <br>�� ������������ <br>���������
					</p>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 reason-block">
					<div class="reason-number">
						9
					</div>
					<p class="reason-content">
						������ � ������ <br>����������<br> � ����� �����
					</p>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 reason-block">
					<div class="reason-number">
						10
					</div>
					<p class="reason-content">
						������ �� ������������� ������ �� ����� �� ����������
					</p>
				</div>
			</div>
		</div>
	</div>
</section>