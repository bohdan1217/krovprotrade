<!-- ������ -->
	<section id="services">
		<div class="container-fluide">
			<h2 class="title_section">���� ������</h2>
			<div class="row fix services-content">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding img-services">
					<img src="<?=SITE_TEMPLATE_PATH?>/img/services.png" alt="services" class="img-serv">
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<p class="text-services">
						��������������� ������������ �������� ������������� ��������� ��������, ������ � <a href="#" class="text-blue">�������� ��������������</a> ��������� ����� ������� � �������. ������,  ������ ���� � ���� �������������� ��������������� ���������� ���������� ������������� ������������� � ������������ � ������������ ����������� � �����������, ��������� � ������ � � ������. ������ �� ������� �������� ������� �� ����� ������� ������, ����������� <a href="#" class="text-blue">����������� ��������� �������</a> � ������� ������� ����� � ���������.
					</p>
					<p class="text-services">
						������� ������� ���������������� � ����������� ��������. ������������� ������������ �������� ����� ��� ����������� � ���������� ��� ���� �����, ������� � ���������� ��������� ������� � �������, ���������� �������� �������� � �����������. �������� �������� ���������� ������� ������ ��������������. 
					</p>
				</div>
			</div>
			<div class="row fix icons-services">
					 <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 ">
						<ul class="flex-icons">
							<li>
								<div class="img-1">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-1.png" alt="" style="width: 30px; height: 31px;">
								</div>
								<p class="list">���������� ������������ � ������ � ������ ���������
								</p>
							</li>
							<li>
								<div class="img-1">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-2.png" alt="" style="width: 30px; height: 25px;">
								</div>
								<p class="list">���������� ����� ����������� �� ������ � �����</p>
							</li>
							<li>
								<div class="img-1">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-3.png" alt="" style="width: 30px; height: 30px;">
								</div>
								<p class="list">������ ������ ��������� �� ��������</p>
							</li>
							<li>
								<div class="img-1">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-4.png" alt="" style="width: 30px; height: 30px;">
								</div>
								<p class="list">�������� � ������</p>
							</li>
						 </ul>
					 </div>
					 <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 right-icons">
						 <ul class="flex-icons">
							<li>
								<div class="img-1">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-5.png" alt="" style="width: 30px; height: 30px;">
								</div>
								<p class="list">����������� �������������</p>
							</li>
							<li>
								<div class="img-1">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-6.png" alt="" style="width: 30px; height: 30px;">
								</div>
								<p class="list">�������� ��������������</p>
							</li>
							<li>
								<div class="img-1">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-7.png" alt="" style="width: 30px; height: 30px;">
								</div>
								<p class="list">����� �������� ��������� �� ��������</p>
							</li>
							<li>
								<div class="img-1">
									<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-8.png" alt="" style="width: 30px; height: 29px;">
								</div>
								<p class="list">���������� ������� ������� �� �������</p>
							</li>
						</ul>

					 </div>
				</div>
			</div>
		</div>
	</section>
<!-- ������ -->

<?/*
<section id="services">
	<div class="container-fluide">
		<h2 class="title_section">���� ������</h2>
		<div class="row fix">
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 no-padding">
				<img src="<?=SITE_TEMPLATE_PATH?>/img/services.png" alt="services">
			</div>
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<p class="text-services">
					��������������� ������������ �������� ������������� ��������� ��������, ������ � <a href="#" class="text-blue">�������� ��������������</a> ��������� ����� ������� � �������. ������,  ������ ���� � ���� �������������� ��������������� ���������� ���������� ������������� ������������� � ������������ � ������������ ����������� � �����������, ��������� � ������ � � ������. ������ �� ������� �������� ������� �� ����� ������� ������, ����������� <a href="#" class="text-blue">����������� ��������� �������</a> � ������� ������� ����� � ���������.
				</p>
				<p class="text-services">
					������� ������� ���������������� � ����������� ��������. ������������� ������������ �������� ����� ��� ����������� � ���������� ��� ���� �����, ������� � ���������� ��������� ������� � �������, ���������� �������� �������� � �����������. �������� �������� ���������� ������� ������ ��������������. 
				</p>
			</div>
		</div>
		<div class="row fix">
				<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
					<ul>
						<li>
							<div class="img-1">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-1.png" alt="" style="width: 30px; height: 31px;">
							</div>
							<p class="list">���������� ������������ � ������ � ������ ���������
							</p>
						</li>
						<li>
							<div class="img-1">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-2.png" alt="" style="width: 30px; height: 25px;">
							</div>
							<p class="list">���������� ����� ����������� �� ������ � �����</p>
						</li>
						<li>
							<div class="img-1">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-3.png" alt="" style="width: 30px; height: 30px;">
							</div>
							<p class="list">������ ������ ��������� �� ��������</p>
						</li>
						<li>
							<div class="img-1">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-4.png" alt="" style="width: 30px; height: 30px;">
							</div>
							<p class="list">�������� � ������</p>
						</li>
					</ul>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 right-icons">
					<ul>
						<li>
							<div class="img-1">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-5.png" alt="" style="width: 30px; height: 30px;">
							</div>
							<p class="list">����������� �������������</p>
						</li>
						<li>
							<div class="img-1">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-6.png" alt="" style="width: 30px; height: 30px;">
							</div>
							<p class="list">�������� ��������������</p>
						</li>
						<li>
							<div class="img-1">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-7.png" alt="" style="width: 30px; height: 30px;">
							</div>
							<p class="list">����� �������� ��������� �� ��������</p>
						</li>
						<li>
							<div class="img-1">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/forma-8.png" alt="" style="width: 30px; height: 29px;">
							</div>
							<p class="list">���������� ������� ������� �� �������</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
</section>
*/?>
