<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="windows-1251" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><?=$APPLICATION->ShowTitle();?></title>

	<!-- Site styles -->
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/lib/css/sliderkit-site.css" media="screen, projection" />

	<?$cur_url = preg_replace('#\?.*#', '', $_SERVER['REQUEST_URI']);?>

	<?/*
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/catalog.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/products_list_style.css" />
	*/?>

	<?if ($cur_url == '/personal/cart/'):?>
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/bascket.css" />
	<?endif;?>
	<?if ($cur_url == '/personal/order/make/'):?>
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/formalization.css" />
	<?endif;?>
	<?if ($cur_url == '/contacts/'):?>
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/contacts.css" />
	<?endif;?>

	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/text-page.css" />

	<?if ($cur_url == '/'):?>
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css" />
	<?endif;?>

	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/menu.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/font-awesome.min.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/fonts.css" />
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/similar-style.css">
	<?if ($cur_url != '/' && $cur_url != '/personal/private/'):?>
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css" />
	<?endif;?>

	<?if ($cur_url == '/personal/subscribe/'):?>
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/subscription.css" />
	<?endif;?>


	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/modal.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/hbmdev.css">


<!--	<link rel="stylesheet" href="--><?//=SITE_TEMPLATE_PATH?><!--/css/card_product_style.css">-->


<!--    <link rel="stylesheet" href="--><?//=SITE_TEMPLATE_PATH?><!--/css/catalog.css" />-->
<!--    <link rel="stylesheet" href="--><?//=SITE_TEMPLATE_PATH?><!--/css/products_list_style.css" />-->





<!--    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!--        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>-->

    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.equalheights.min.js"></script>

    <script src="<?=SITE_TEMPLATE_PATH?>/js/basket.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/hbmdev.js"></script>



    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- Optional theme -->
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<!-- Latest compiled and minified JavaScript -->
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

	<script src="<?=SITE_TEMPLATE_PATH?>/js/bxslider/jquery.bxslider.min.js"></script>
	<!-- bxSlider CSS file -->
	<link href="<?=SITE_TEMPLATE_PATH?>/js/bxslider/jquery.bxslider.min.css" rel="stylesheet" />

	<link href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox.min.css" rel="stylesheet" />
	<script src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox.min.js"></script>

	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/lib/js/external/_oldies/jquery-1.3.min.js"></script>

	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/lib/js/external/jquery.easing.1.3.min.js"></script>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/lib/js/external/jquery.mousewheel.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/lib/js/external/lightbox/jquery-lightbox/js/jquery.lightbox-0.5.pack.js"></script>

		<!-- Slider Kit scripts -->
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/lib/js/sliderkit/jquery.sliderkit.1.9.2.pack.js"></script>



		<!-- Lightbox styles -->
		<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/lib/js/external/lightbox/jquery-lightbox/css/jquery.lightbox-0.5.css" media="screen, projection" />

	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/lib/css/sliderkit-core.css" media="screen, projection" />
		<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/lib/css/sliderkit-demos.css" media="screen, projection" />

	<?$APPLICATION->ShowHead();?>

	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/template_script.js');?>

</head>
<body>
	<!--header-->

	<div id="panel">
		<?$APPLICATION->ShowPanel();?>
	</div>

	<section id="header">
		<div class="container-fluide">
			<div class="row fix">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
					<?$APPLICATION->IncludeComponent(
                            "bitrix:sale.basket.basket.line",
                            "mini_cart",
                            array(
                                "HIDE_ON_BASKET_PAGES" => "Y",
                                "PATH_TO_BASKET" => SITE_DIR."personal/cart/",
                                "PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
                                "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                                "PATH_TO_PROFILE" => SITE_DIR."personal/",
                                "PATH_TO_REGISTER" => SITE_DIR."login/",
                                "POSITION_FIXED" => "N",
                                "SHOW_AUTHOR" => "N",
                                "SHOW_EMPTY_VALUES" => "Y",
                                "SHOW_NUM_PRODUCTS" => "Y",
                                "SHOW_PERSONAL_LINK" => "Y",
                                "SHOW_PRODUCTS" => "N",
                                "SHOW_TOTAL_PRICE" => "Y",
                                "COMPONENT_TEMPLATE" => "mini_cart"
                            ),
                            false
                        );?>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
					<?$APPLICATION->IncludeComponent(
                            "bitrix:system.auth.form",
                            "header_authorize",
                            array(
                                "FORGOT_PASSWORD_URL" => "",
                                "PROFILE_URL" => "",
                                "REGISTER_URL" => "",
                                "SHOW_ERRORS" => "N",
                                "COMPONENT_TEMPLATE" => "header_authorize"
                            ),
                            false
                        );?>
				</div>
			</div>
		</div>
	</section>

	<section id="info-block">
		<div class="container-fluide">
			<div class="row fix">
				<div class="flex-container">
					<div class="info-blocks info-logo">
						<a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/img/logo-blue.png" alt="logo"></a>
					</div>

					<div class="info-blocks time">
						<img src="<?=SITE_TEMPLATE_PATH?>/img/imgpsh_fullsize (1).png" alt="time">
						<div class="time-text">
							<p class="info-upper">����� ������: </p>
							�� � �� � 9:00 �� 18:00
						</div>
						<div class="time-text_adaptiv">
							<a href="tel:8 (495) 646-75-10" class="tellto">8 (495) 646-75-10</a>
							<p>�� � �� � 9:00 �� 18:00</p>
							<a href="mailto:8829945@mail.ru" class="info-upper">8829945@mail.ru</a>
						</div>
					</div>

					<div class="info-blocks mail">
						<img src="<?=SITE_TEMPLATE_PATH?>/img/imgpsh_fullsize.png" alt="btn" class="mailto">
						<div class="mailto">
							<p class="info-upper">e-mail:</p>
							<a href="mailto:8829945@mail.ru" class="info-upper">8829945@mail.ru</a>
						</div>
					</div>

					<div class="info-blocks tell">
						<div class="info_block-tel">
							<a href="tel:8 (495) 646-75-10" class="telto">8 (495) 646-75-10</a>
							<a href="#win3">
								<button>�������� ������</button>
							</a>
						</div>

						<div class="info-icons">
							<div class="flex-container">
								<a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/img/search-black.png" alt=""></a>
								<a href="tel:8 (495) 646-75-10"><img src="<?=SITE_TEMPLATE_PATH?>/img/call-black.png" alt=""></a>
								<a href="?logout=yes"><img src="<?=SITE_TEMPLATE_PATH?>/img/exit-black.png" alt=""></a>
								<a href="/personal/cart/"><img src="<?=SITE_TEMPLATE_PATH?>/img/basket-black.png" alt=""></a>
							</div>
						</div>
					</div>

					<!-- ��������� ���� 3 -->
					<a href="#x" class="overlay" id="win3"></a>
					<div class="popup input-bg">
						<a class="close" title="�������" href="#close"></a>

							<?$APPLICATION->IncludeComponent("bitrix:main.feedback","callback_form",Array(
								"USE_CAPTCHA" => "N",
								"OK_TEXT" => "�������, ���� ��������� �������.",
								"EMAIL_TO" => "polichinelle1991@gmail.com",
								"REQUIRED_FIELDS" => array(),
								"EVENT_MESSAGE_ID" => Array("100", "FEEDBACK")
							)
						);?>
					</div>

				</div>
			</div>
		</div>
	</section>

	<? if ($_SERVER['REQUEST_URI'] == '/'): ?>
		<div class="slider_menu">
		<?$APPLICATION->IncludeComponent("bitrix:menu", "main_top_menu", Array(
		"ALLOW_MULTI_SELECT" => "N",	// ��������� ��������� �������� ������� ������������
			"CHILD_MENU_TYPE" => "left",	// ��� ���� ��� ��������� �������
			"DELAY" => "N",	// ����������� ���������� ������� ����
			"MAX_LEVEL" => "1",	// ������� ����������� ����
			"MENU_CACHE_GET_VARS" => "",	// �������� ���������� �������
			"MENU_CACHE_TIME" => "3600",	// ����� ����������� (���.)
			"MENU_CACHE_TYPE" => "N",	// ��� �����������
			"MENU_CACHE_USE_GROUPS" => "Y",	// ��������� ����� �������
			"ROOT_MENU_TYPE" => "top",	// ��� ���� ��� ������� ������
			"USE_EXT" => "N",	// ���������� ����� � ������� ���� .���_����.menu_ext.php
			"COMPONENT_TEMPLATE" => ".default"
		),
		false
	);?>

			<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "main", Array(
                "PATH" => "",	// ����, ��� �������� ����� ��������� ������������� ������� (�� ���������, ������� ����)
                    "SITE_ID" => "s1",	// C��� (��������������� � ������ ������������� ������, ����� DOCUMENT_ROOT � ������ ������)
                    "START_FROM" => "0",	// ����� ������, ������� � �������� ����� ��������� ������������� �������
                ),
                false
            );?>


            <!-- �������  -->
            <?$APPLICATION->IncludeComponent("bitrix:news.list", "slider", Array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// ������ ������ ����
                    "ADD_SECTIONS_CHAIN" => "N",	// �������� ������ � ������� ���������
                    "AJAX_MODE" => "N",	// �������� ����� AJAX
                    "AJAX_OPTION_ADDITIONAL" => "",	// �������������� �������������
                    "AJAX_OPTION_HISTORY" => "N",	// �������� �������� ��������� ��������
                    "AJAX_OPTION_JUMP" => "N",	// �������� ��������� � ������ ����������
                    "AJAX_OPTION_STYLE" => "Y",	// �������� ��������� ������
                    "CACHE_FILTER" => "N",	// ���������� ��� ������������� �������
                    "CACHE_GROUPS" => "Y",	// ��������� ����� �������
                    "CACHE_TIME" => "36000000",	// ����� ����������� (���.)
                    "CACHE_TYPE" => "A",	// ��� �����������
                    "CHECK_DATES" => "Y",	// ���������� ������ �������� �� ������ ������ ��������
                    "DETAIL_URL" => "",	// URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
                    "DISPLAY_BOTTOM_PAGER" => "Y",	// �������� ��� �������
                    "DISPLAY_DATE" => "Y",	// �������� ���� ��������
                    "DISPLAY_NAME" => "Y",	// �������� �������� ��������
                    "DISPLAY_PICTURE" => "Y",	// �������� ����������� ��� ������
                    "DISPLAY_PREVIEW_TEXT" => "Y",	// �������� ����� ������
                    "DISPLAY_TOP_PAGER" => "N",	// �������� ��� �������
                    "FIELD_CODE" => array(	// ����
                        0 => "",
                        1 => "",
                    ),
                    "FILTER_NAME" => "",	// ������
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// �������� ������, ���� ��� ���������� ��������
                    "IBLOCK_ID" => "8",	// ��� ��������������� �����
                    "IBLOCK_TYPE" => "settings",	// ��� ��������������� ����� (������������ ������ ��� ��������)
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// �������� �������� � ������� ���������
                    "INCLUDE_SUBSECTIONS" => "Y",	// ���������� �������� ����������� �������
                    "MESSAGE_404" => "",	// ��������� ��� ������ (�� ��������� �� ����������)
                    "NEWS_COUNT" => "20",	// ���������� �������� �� ��������
                    "PAGER_BASE_LINK_ENABLE" => "N",	// �������� ��������� ������
                    "PAGER_DESC_NUMBERING" => "N",	// ������������ �������� ���������
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// ����� ����������� ������� ��� �������� ���������
                    "PAGER_SHOW_ALL" => "N",	// ���������� ������ "���"
                    "PAGER_SHOW_ALWAYS" => "N",	// �������� ������
                    "PAGER_TEMPLATE" => ".default",	// ������ ������������ ���������
                    "PAGER_TITLE" => "�������",	// �������� ���������
                    "PARENT_SECTION" => "",	// ID �������
                    "PARENT_SECTION_CODE" => "",	// ��� �������
                    "PREVIEW_TRUNCATE_LEN" => "",	// ������������ ����� ������ ��� ������ (������ ��� ���� �����)
                    "PROPERTY_CODE" => array(	// ��������
                        0 => "",
                        1 => "DESC",
                        2 => "LINK",
                        3 => "TEXT",
                        4 => "TITLE",
                        5 => "",
                    ),
                    "SET_BROWSER_TITLE" => "N",	// ������������� ��������� ���� ��������
                    "SET_LAST_MODIFIED" => "N",	// ������������� � ���������� ������ ����� ����������� ��������
                    "SET_META_DESCRIPTION" => "N",	// ������������� �������� ��������
                    "SET_META_KEYWORDS" => "N",	// ������������� �������� ����� ��������
                    "SET_STATUS_404" => "N",	// ������������� ������ 404
                    "SET_TITLE" => "N",	// ������������� ��������� ��������
                    "SHOW_404" => "N",	// ����� ����������� ��������
                    "SORT_BY1" => "ACTIVE_FROM",	// ���� ��� ������ ���������� ��������
                    "SORT_BY2" => "SORT",	// ���� ��� ������ ���������� ��������
                    "SORT_ORDER1" => "DESC",	// ����������� ��� ������ ���������� ��������
                    "SORT_ORDER2" => "ASC",	// ����������� ��� ������ ���������� ��������
                    "COMPONENT_TEMPLATE" => ".default"
                ),
                false
            );?>
		</div>
	<?else:?>
		<div class="menu">
		<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"main_top_menu",
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "main_top_menu"
	),
	false
);?>
		</div>

		<?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "main",
                array(
                    "PATH" => "",
                    "SITE_ID" => "s1",
                    "START_FROM" => "0",
                    "COMPONENT_TEMPLATE" => "main"
                ),
                false
            );?>
	<?endif;?>