function call() {
    var form = $('.formx');
    var msg   = form.serializeArray();
    //console.log(msg);
    var form_result = form.find('.form_result');
    $.ajax({
        type: 'POST',
        url: '/bitrix/templates/krovprotrade/ajax/feedback_form.php',
        data: msg,
        async : true,
        success: function(data) {
            if(data == 1){
                //alert();
                //$('#loading').fadeIn('fast');
                //location.reload();
                form.trigger('reset');
                window.location.href = '#thank';
            }else{
                form_result.html(data);
            }
        },
        error:  function(xhr, str){
            form_result.html(data);
        }
    });
}

function restore_password() {
    var form = $('#renewModalForm');
    var msg   = form.serializeArray();
    //console.log(msg);
    var form_result = form.find('.form_result_renewModal');
    $.ajax({
        type: 'POST',
        url: '/bitrix/templates/krovprotrade/ajax/forget_password.php',
        data: msg,
        async : true,
        success: function(data) {
            //console.log(data);
            if(data == 1){
                //alert();
                //$('#loading').fadeIn('fast');
                //location.reload();
                form.trigger('reset');
                window.location.href = '#thank_password';
            }else{
                form_result.html(data);
            }
        },
        error:  function(xhr, str){
            form_result.html(data);
        }
    });
}


//$('.equalheight .bg_sells').equalHeights();
//$('.equalheight shingles').css('display','none');

 $(document).ready(function() {
     //$('.equalheight > .sell-block').equalHeights();
     //$(".sell-block").css("background-color", "yellow");
     //$(".sell-block div").equalHeights();;
     // $(".equalheight .sell-block").css("background-color", "yellow");
     //$(".equalheightnews div").equalHeights();
 });

// $(document).ready(function() {
//     $('.minus').click(function () {
//         var $input = $(this).parent().find('input');
//         var count = parseInt($input.val()) - 1;
//         count = count < 1 ? 1 : count;
//         $input.val(count);
//         $input.change();
//         return false;
//     });
//     $('.plus').click(function () {
//         var $input = $(this).parent().find('input');
//         $input.val(parseInt($input.val()) + 1);
//         $input.change();
//         return false;
//     });
// });
