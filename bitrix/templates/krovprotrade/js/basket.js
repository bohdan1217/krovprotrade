/*$("input[name='DELIVERY_ID']").change(function(){
	$("#loading").fadeIn('fast');
	var price = parseFloat($(this).data('price'));
	var name = $(this).data('name');
	$("#sw_delivery_total").text(price + ' ���.').data('original', price);
	var total = parseFloat($("#basket_super_total").text());
	var discount = 0;
	if($("#promo_discount_value").length > 0)
		discount = parseFloat($("#promo_discount_value").text());
	$("#sw_total_with_discount").text(price + total - discount);
	$("#cart_delivery_name").text(name);
	$("#loading").fadeOut('fast');
})*/

function sw_remove_from_basket($item){

    $.ajax({
        url:'/bitrix/templates/krovprotrade/ajax/basket/delete_from_basket.php',
        type: 'POST',
        async : true,
        data: {
            item:$item
        },
        beforeSend: function(){
            $('#loading').fadeIn('fast');
        },
        success: function(data){
            $("#basket_item_" + $item).remove();

            var basket_counter = $("#top_basket_counter");
            var basket_counter_cart = $("#top_basket_counter_cart");


            $basket_counter = parseInt(basket_counter.text());
            $basket_counter_cart = parseInt(basket_counter_cart.text());

            basket_counter.text(($basket_counter - 1));
            basket_counter_cart.text(($basket_counter_cart - 1));

            if($basket_counter == 1 || $item == 'all'){
                $("#checkout").html('	<div class="no_products"><h4>���� ������� �����</h4></div>');
            }
            //$("#header_basket_line").replaceWith(data);
            sw_calculate_basket_total();

            $('#loading').fadeOut('fast');
        },
        error: function(){
            $('#loading').fadeOut('fast');
        }
    });
}

function sw_set_item_quantity($item, $quantity){

    sw_calculate_basket_total();
    //$("#basket_super_discount").text(Math.round($total_discount));

    $.ajax({
        url:'/bitrix/templates/krovprotrade/ajax/basket/set_item_quantity.php',
        type: 'POST',
        async : true,
        data: {
            item:$item,
            quantity:$quantity
        },
        beforeSend: function(){
            //$('#preloader').fadeIn('fast');
        },
        success: function(data){
            //$("#header_basket_line").replaceWith(data);
            //$('#preloader').fadeOut('fast');
        },
        error: function(){
            //$('#preloader').fadeOut('fast');
        }
    });
}

function sw_calculate_basket_total(){
    var $total = 0;
    var $total_all = 0;
    var $nds = 0;
    //var $total_discount = 0;


    $(".total_span").each(function(){
        var th = $(this);

        $total += parseFloat(th.attr('data-total'));
        $nds += parseFloat(th.attr('data-nds'));

        //console.log(parseFloat(th.attr('data-total')));
        //$total_all += parseFloat(th.attr('data-total_all'));
        //var $span_id = th.attr('id').split('_').pop();

        //var $qau = parseInt($("#NUMBER_FIELD_" + $span_id).val() );

        //$total_discount += (parseFloat(th.data('full_price')) * parseFloat(th.data('discount_percent')) * $qau) / 100;
    });


    //$("#basket_super_total").text(number_format($total_all, 0, '.', ' '));
    $("#basket_total").text(number_format($total, 0, '.', ' '));
    $("#basket_total_all").text(number_format($total_all, 0, '.', ' '));

    var delivery_price = 0;
    var delivery_pirce_input_val = parseFloat($("#delivery_price_input").val());
    if(delivery_pirce_input_val)
        delivery_price = delivery_pirce_input_val;

    $("#basket_total_plus_nds").text(number_format(($total + delivery_price), 0, '.', ' '));
    $("#basket_nds").text(number_format($nds, 1, '.', ' '));


    //var $delivery_price = parseFloat($("#sw_delivery_total").text());
    //var $over_price = $total + $delivery_price;
    //$("#sw_total_with_delivery").text($over_price);

    /*if($total_all != $total)
        $("#promo_discount_value").text(($total_all - $total)); */
}

function sw_buy_one_click(selector){
    $(selector).click(function(){
        var id = $(this).data('id');
        var img = $(this).data('img');
        $.ajax({
            url: '/bitrix/templates/krovprotrade/ajax/buy_cheeper.php',
            type: 'POST',
            async : true,
            data: {
                id:id,
                img:img,
            },
            beforeSend: function(){
                $('#loading').fadeIn('fast');
            },
            success: function(data){
                $(".modal").modal('hide');
                setTimeout(function(){
                    $("#sw_one_click_product_id").attr('value', id);
                    $("#buy_one_click_img").attr('src', img);
                    $("#sw_buy_one_click_ajax_result").html(data);
                    $("#modalBuyCheaperOnClick").modal('show');
                    sw_cheeper_functions();
                }, 400);

                $('#loading').fadeOut('fast');
            },
            error: function(){
                $('#loading').fadeOut('fast');
            }
        });
        return false;
    });
}
function sw_basket_functions(wrapper){

    /*$(".modal  #continue_shopping").click(function(){
        $(".modal").modal('hide');
        return false;
    });*/

    if(!wrapper)
        wrapper = '';



    $(wrapper + ' .pluser__num').on('keyup', function (e){
        var $th = $(this);
        var $val = parseInt($th.val());
        if ($val> 1){
            var $item = parseInt($th.data('item'));
            var total_price = parseFloat($("#price_" + $item).data('price')) * ($val);
            //console.log(total_price);

            $("#total_price_" + $item).text(number_format(total_price, 0, '.', ' ')).attr('data-total', total_price);

            $th.val($val);

            sw_set_item_quantity($item, $val);
        };
        e.preventDefault();
    });


    $(wrapper + ' .minus_basket').on('click', function (e){
        var $th = $(this);
        var $quantityInput = $th.siblings('input');
        var $val = parseInt($quantityInput.val());

        if ($val> 1){
            var $item = parseInt($th.data('item'));

            //var total_nds = parseFloat($("#total_price_" + $item).data('nds')) - ($val - 1);

            var total_price = parseFloat($("#price_" + $item).data('price')) * ($val - 1);
            var total_price_old = parseFloat($("#price_" + $item).data('price_old')) * ($val - 1);

            //$("#total_price_" + $item).text(number_format(total_nds, 0, '.', ' ')).attr('data-nds', total_nds);




            $("#total_price_" + $item).text(number_format(total_price, 0, '.', ' ')).attr('data-total', total_price);
            //$("#total_price_" + $item).text(number_format(total_price_old, 0, '.', ' ')).attr('data-total_all', total_price_old);
            $("#total_price_old_" + $item).text(number_format(total_price_old, 0, '.', ' ')).attr('data-total_all', total_price_old);

            $quantityInput.val($val - 1);

            sw_set_item_quantity($item, $val - 1);
        };
        e.preventDefault();
    });





    $(wrapper + ' .plus_basket').on('click', function (e){
        var $th = $(this);
        var $quantityInput = $th.siblings('input');
        var $val = parseInt($quantityInput.val());
        var $item = parseInt($th.data('item'));

        var total_nds = parseFloat($("#nds_" + $item).data('nds')) * ($val + 1);

        var total_price = parseFloat($("#price_" + $item).data('price')) * ($val + 1);
        var total_price_old = parseFloat($("#price_" + $item).data('price_old')) * ($val + 1);
        //console.log(total_price_old);



        $("#nds_" + $item).text(number_format(total_nds, 0, '.', ' ')).attr('data-nds', total_nds);
        $("#total_price_" + $item).text(number_format(total_price, 0, '.', ' ')).attr('data-total', total_price);
        $("#total_price_old_" + $item).text(number_format(total_price_old, 0, '.', ' ')).attr('data-total_all', total_price_old);

        $quantityInput.val($val + 1);

        sw_set_item_quantity($item, $val + 1);
        e.preventDefault();
    });


    $(wrapper + " .ico-remove").click(function(e){
        var item = $(this).data('item');
        sw_remove_from_basket(item);
        e.preventDefault();
    });

    /*$("input[name='ORDER_PROP_4']").on('change', function(){
        setTimeout(function(){
            sw_calculate_basket_total();
        }, 400);
    })*/

    /*$("input[name='DELIVERY_ID']").on('click', function(){
        var th = $(this);
        var price = th.data('price');
        var delivery_name = th.next().find('.payment__item span').text();

        $("#sw_delivery_total").text(price);
        $("#delivery_name").text(delivery_name);
        sw_update_payment_methods();
        sw_calculate_basket_total();
    });
    $("input[name='PAY_SYSTEM_ID']").on('click', function(){
        var th = $(this);
        var payment_name = th.next().find('.payment__item span').text();
        $("#sw_payment_name").text(payment_name);
    });*/

    /*$("#sw_delete_all_items").click(function(e){
        sw_remove_from_basket('all');

        e.preventDefault();
    });
*/

    $("#set_coupon").click(function(e){
        var $coupon = $.trim($("#sw_coupon_field").val());
        //console.log($coupon);
        if($coupon == ''){
            $("#sw_coupon_field").addClass('error');
            return false;
        }

        //var coupon = $.trim($("#sw_coupon_field").val());

        $.ajax({
            url:'/bitrix/templates/krovprotrade/ajax/basket/set_coupon.php',
            type: 'POST',
            async : true,
            data: {
                coupon:$coupon
            },
            //processData: false,
            //contentType: false,
            beforeSend: function(){
                $('#loading').fadeIn('fast');
            },
            success: function(data){
                //console.log($coupon);
                if(data == 1){
                    $("#coupon_result").html('<div class="error-message-success">�������� ������� �����������. �������� �������� ��� �� ������� ��������� � �������.</div>');
                }else{
                    $("#coupon_result").html('<div class="error-message">�������� �������������� ��� ���������� �� � ������ �� ������� � ����� �������.</div>');
                }
                $('#loading').fadeOut('fast');
            },
            error: function(){
                $('#loading').fadeOut('fast');
            }
        });
        return false;
    });
}


function sw_add_to_cart(selector){
    var new_selector = selector.replace('ajax_loaded', 'ajax_events');
    $(new_selector).click(function(){
        var button = $(this);
        var form = $(this).closest('form');

        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            async : true,
            data: form.serialize(),
            beforeSend: function(){
                $('#loading').fadeIn('fast');
            },
            success: function(data){

                $("#header_basket").html(data);

                var basket_coutner = parseInt($("#modal_basket_counter").text());
                $("#top_basket_counter").text(basket_coutner);
                //$("#basket_counter_cart").text(basket_coutner);


                //var bascket_price = parseInt($("#modal_basket_total").text());
                //$("#top_basket_price").text(bascket_price +  ' ���.');


                //var name = button.data('name');
                //$("#modal_product_name").text(name);

                //$("#header_basket_modal").modal('show');

                //$("#item_popup").fadeOut('fast');

                $('.day-popup-bg').fadeIn(400);
                $('.popup-cart').fadeIn();

                $('#loading').fadeOut('fast');


                window.location.href = '#modal_cart';

            },
            error: function(){
                $('#loading').fadeOut('fast');
            }
        });
        return false;
    });
}


function sw_load_basket(){
    $.ajax({
        url: '/bitrix/templates/krovprotrade/ajax/basket.php',
        type: 'GET',
        async : true,
        data: {

        },
        beforeSend: function(){
            $('#loading').fadeIn('fast');
        },
        success: function(data){
            $("#add_to_cart_result").html(data);
            sw_basket_functions('#add_to_cart_result');
            $("#cartModal").modal('show');
            $('#loading').fadeOut('fast');
        },
        error: function(){
            $('#loading').fadeOut('fast');
        }
    });
}

///// init
$(function(){
    if($("#checkout").length > 0){
        sw_basket_functions();
    }
    //header_basket_functions();

    sw_add_to_compare('.add_to_compare');
    sw_add_to_fav('.add_to_fav');
    sw_add_to_cart('.add_to_cart');
})


// $("#location_4_select").on('change', function(){
// 	var th = $(this);
// 	var val = th.val();
// 	var price = th.data('price');
// 	var weight = th.data('weight');

// 	$.ajax({
//         url: '/bitrix/templates/krovprotrade/ajax/basket/delivery_by_location.php',
//         type: 'GET',
//         async : true,
//         data: {
//         	location:val,
//         	price:price,
//         	weight:weight
//         },
//         beforeSend: function(){
//           $('#loading').fadeIn('fast');
//         },
//         success: function(data){
//         	$("#delivery_result").html(data);
//         	var delivery_input = $("input[name='DELIVERY_ID']:checked");

//         	var delivery_price = delivery_input.data('price');
//         	var delivery_name = delivery_input.next().find('.payment__item span').text();

//         	$("#delivery_name").text(delivery_name);
//         	$("#sw_delivery_total").text(delivery_price);

//             sw_update_payment_methods();
//             sw_calculate_basket_total();
//             $('#loading').fadeOut('fast');
//         },
//         error: function(){
//           $('#loading').fadeOut('fast');
//         }
//     });
// });


function sw_update_payment_methods(){
    var delivery = $("input[name='DELIVERY_ID']:checked").val();

    $.ajax({
        url: '/bitrix/templates/krovprotrade/ajax/basket/payment_by_location.php',
        type: 'GET',
        async : true,
        data: {
            delivery:delivery
        },
        beforeSend: function(){
            $('#loading').fadeIn('fast');
        },
        success: function(data){
            $("#payment_result").html(data);
            $('#loading').fadeOut('fast');
        },
        error: function(){
            $('#loading').fadeOut('fast');
        }
    });
}

// $('#ORDER_FORM').validate({
// 	errorClass:'error-input',
// 	validClass:'success',
// 	highlight: function (element, errorClass, validClass) {
// 		$(element).closest('.modal__input-wrap').addClass(errorClass).removeClass(validClass);
// 	},
// 	unhighlight: function (element, errorClass, validClass) {
// 		$(element).closest('.modal__input-wrap').removeClass(errorClass).addClass(validClass);
// 	},
// 	errorPlacement: function(error, element) { },
// 	rules: {

// 	},
// 	submitHandler:function(form){
//     	$('#loading').fadeIn('fast');
//     	return true;
//     }
// });


function number_format( number, decimals, dec_point, thousands_sep ) {	// Format a number with grouped thousands
    //
    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +	 bugfix by: Michael White (http://crestidg.com)

    var i, j, kw, kd, km;

    // input sanitation & defaults
    if( isNaN(decimals = Math.abs(decimals)) ){
        decimals = 2;
    }
    if( dec_point == undefined ){
        dec_point = ",";
    }
    if( thousands_sep == undefined ){
        thousands_sep = ".";
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if( (j = i.length) > 3 ){
        j = j % 3;
    } else{
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


    return km + kw + kd;
}

function sw_add_to_fav(selector){
    $(selector).click(function(){
        var button = $(this);
        var product_id = button.data('id');
        var remove = 0;
        if(button.hasClass('active') || button.hasClass('remove') )
            remove = 1;
        var parent_id = null;
        if(button.attr('data-parent'))
            parent_id = button.attr('data-parent');

        $.ajax({
            url: '/bitrix/templates/krovprotrade/ajax/add_to_fav.php',
            type: 'GET',
            async : true,
            data: {
                product_id:product_id,
                parent_id:parent_id,
                remove:remove
            },
            /*  processData: false,
                contentType: false,*/
            beforeSend: function(){
                $('#loading').fadeIn('fast');
            },
            success: function(data){

                if(!isNaN(data)){
                    if(remove){
                        if(parseInt(data) == 0){
                            $("#favorites_wrapper").html('   <div class="no_products_likes"><h4>� ��� ��� ��������� �������</h4></div>');
                        }

                        if(button.hasClass('remove'))
                            button.closest('.favorite_item').remove();
                        else
                            button.removeClass('active');
                    }
                    else{
                        button.addClass('active');
                    }
                    $("#favorites_counter").text(data);
                }
                $('#loading').fadeOut('fast');
            },
            error: function(){
                $('#loading').fadeOut('fast');
            }
        });
        return false;
    });
}

function sw_add_to_compare(selector){
    $(selector).click(function(){
        var button = $(this);
        var id = button.data('id');
        var action = 'ADD_TO_COMPARE_LIST';
        var remove = 0;
        var data = {
            action:action,
            id:id
        };

        if(button.hasClass('active') || button.hasClass('compare__delete')){
            action = 'DELETE_FROM_COMPARE_LIST';
            remove = 1;
            data = {
                action:action,
                ID:id
            };
        }
        console.log(button.attr('class'));

        var compare_counter = parseInt($("#compare_counter").text());


        $.ajax({
            url: '/compare/',
            type: 'GET',
            async : true,
            data: data,
            /*  processData: false,
                contentType: false,*/
            beforeSend: function(){
                $('#loading').fadeIn('fast');
            },
            success: function(data){
                if(remove){
                    compare_counter -= 1;
                    if($(".__img-td[data-product='"+ id +"']").length> 0){
                        $(".__img-td[data-product='"+ id +"'], .__tr-option[data-product='"+ id +"']").remove();
                        /*    	$('.compare__table-wrap').perfectScrollbar({
                                    suppressScrollY: true
                                });*/
                        $('.compare__table-wrap').perfectScrollbar('update');
                        $('.compare__table-wrap').perfectScrollbar('update');
                        //	Ps.update('.compare__table-wrap');
                    }
                    if(compare_counter == 0){
                        $("#compare_wrapper").html('<p><font class="notetext">������ ������������ ��������� ����.</font></p>');
                    }else{
                        button.removeClass('active');
                    }
                }else{

                    compare_counter += 1;
                    button.addClass('active');
                }
                $("#compare_counter").text(compare_counter);

                $('#loading').fadeOut('fast');
            },
            error: function(){
                $('#loading').fadeOut('fast');
            }
        });
        return false;
    });
}

$("#order_prop_4").on('change', function(){
    var region = $(this).val();

    $.ajax({
        url: '/bitrix/templates/babycat/ajax/locations_get_city.php',
        type: 'POST',
        async : true,
        data: {region:region},
        beforeSend: function(){
            $('#loading').fadeIn('fast');
        },
        success: function(data){
            $("#order_prop_5").html(data).removeAttr('disabled');

            $('#loading').fadeOut('fast');
        },
        error: function(){
            $('#loading').fadeOut('fast');
        }
    });
})


