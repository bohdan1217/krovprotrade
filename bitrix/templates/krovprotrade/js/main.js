$(function(){
	//show-hide forms
	$('#show_login').click(function(){
		$('.login_form_wrapper').animate({
			top: '177px'
		}, 300);
		$('.main_overlay').css('display', 'block').animate({
			opacity: '0.4'
		}, 300);
		$('body').css('overflow', 'hidden');
	});
	$('#show_reg').click(function(){
		$('.reg_form_wrapper').animate({
			top: '177px'
		}, 300);
		$('.main_overlay').css('display', 'block').animate({
			opacity: '0.4'
		}, 300);
		$('body').css('overflow', 'hidden');
	});
	$('.form_close_button').click(function(){
		$(this).closest('.outer_wrapper').animate({
			top: '-700px'
		}, 300);
		$('.main_overlay').animate({
			opacity: '0.4'
		}, 300, function(){
			$('.main_overlay').css('display', 'none');
		});
		$('body').css('overflow', 'auto');
	});
	$(document).on('click', '.login_form_reg_link', function(){
		$('.login_form_wrapper').animate({
			top: '-700px'
		}, 300, function(){
			if ($(window).width() > 1200) {
				$('.reg_form_wrapper').animate({
					top: '177px'
				}, 300);
			} else {
				$('.reg_form_wrapper').animate({
					top: '0px'
				}, 300);
			};
			
		});
		return false;
	});

	//show-hide mob.forms
	$(document).on('click', '.mob_header_login_btn', function(){
		$('.login_form_wrapper').animate({
			top: '0px'
		}, 300);
		$('.main_overlay').css('display', 'block').animate({
			opacity: '0.4'
		}, 300);
		$('body').css('overflow', 'hidden');
	});

	//show-hide search field
	$('.header_search_overlay').click(function(){
		$(this).css('display', 'none');
		$('.header_search_wrapper input[type="text"]').animate({
			width: '260px',
			padding: '0px 8px'
		}, 200).focus();
		setTimeout(function(){
			if ($('.header_search_wrapper input[type="text"]').val().length == 0) {
				$('.header_search_wrapper input[type="text"]').animate({
					width: '0px',
					padding: '0px'
				}, 200);
				$('.header_search_overlay').css('display', 'block');
			};
		}, 3000);
	});

	//show-hide main_menu 
	$(document).on('click', '.mob_header_menu_btn', function(){
		if ($('.main_menu_wrapper').css('display') == 'none') {
			$('.main_menu_wrapper').slideDown(300);
		} else {
			$('.main_menu_wrapper').slideUp(300);
		};
	});

	//show-hide mob.search
	$(document).on('click', '.mob_header_search_btn', function(){
		if ($(window).width() <= 1200 && $('.mob_header_search_wrapper').css('display') == 'none') {
			$('.mob_header_search_wrapper').slideDown(300).find('input[type="text"]').focus();
		} else if ($(window).width() <= 1200 && $('.mob_header_search_wrapper').css('display') == 'block') {
			$('.mob_header_search_wrapper').slideUp(300);
		};
	});

	//reg.form text inputs
	$('.popup_reg_form_fields input[type="text"], .popup_reg_form_fields input[type="password"]').focus(function(){
		$(this).css('background', '#fff');
	});
	$('.popup_reg_form_fields input[type="text"], .popup_reg_form_fields input[type="password"]').blur(function(){
		if ($(this).val().length > 0) {
			$(this).css('background', '#fff');
		} else {
			$(this).css('background', 'none');
		};
	});

	//reg.form img click
	$('.popup_captcha_img').click(function(){
		$('.popup_captcha_input input').focus();
	});

	//footer path map
	$('a.path_map_link').click(function(){
		return false;
	});

	//mainpage our work slider
	if ($(window).width() < 1201 && $(window).width() > 768) {
		var mainPageOurWork = $('#mainpage_our_works').lightSlider({
			gallery:false,
	        item:3,
	        auto:false,
	        slideMargin:0,
	        slideMove:1,
	        controls:false,
	        pager:false,
	        enableTouch:true,
	        enableDrag:true
		});
		$('.mainpage_our_works_prev').click(function(){
	        mainPageOurWork.goToPrevSlide(); 
	    });
	    $('.mainpage_our_works_next').click(function(){
	        mainPageOurWork.goToNextSlide(); 
	    });
	} else if ($(window).width() < 769) {
		var mainPageOurWork = $('#mainpage_our_works').lightSlider({
			gallery:false,
	        item:1,
	        auto:false,
	        slideMargin:0,
	        slideMove:1,
	        controls:false,
	        pager:false,
	        enableTouch:true,
	        enableDrag:true
		});
		$('.mainpage_our_works_prev').click(function(){
	        mainPageOurWork.goToPrevSlide(); 
	    });
	    $('.mainpage_our_works_next').click(function(){
	        mainPageOurWork.goToNextSlide(); 
	    });
	};
});

