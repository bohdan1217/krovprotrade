$(document).ready(function () {
	$('.form_wrap .plus, .form_wrap .minus').click(function () {
		var elem = $(this);
		var block = $(this).parents('.form_wrap');
		var quantity_input = block.find('[name=quantity]');
		if (quantity_input.length == 0) {
			quantity_input = block.find('[name^=QUANTITY]');
		}
		var quantity_delta;
		if (elem.hasClass('plus')) {
			quantity_delta = +1;
		} else if (elem.hasClass('minus')) {
			quantity_delta = -1;
		}
		var quantity = parseInt(quantity_input.val());
		var quantity_min = parseInt(quantity_input.attr('data-min'));
		var quantity_max = parseInt(quantity_input.attr('data-max'));
		
		quantity = quantity ? quantity + quantity_delta : 1;
		
		if (quantity < quantity_min) {
			quantity = quantity_min;
		}
		if (quantity > quantity_max) {
			quantity = quantity_max;
		}
		quantity_input.val(quantity);
		
		return false;
	});
	
	$('.basket_form .cancel').click(function () {
		var elem = $(this);
		var form = elem.parents('form');
		var id_input = form.find('[name=id]');
		id_input.val(elem.attr('data-id'));
		
		return true;
	});

});