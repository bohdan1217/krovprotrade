<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<!-- contacts -->
	<section id="contacts">
		<div class="container-fluide">
			<div class="row fix contacts">
				<div class="contacts-blocks contacts-work">
					<div class="row">�������: <a href="tel:8 (495) 646-75-10">8 (495) 646-75-10</a></div>
					<div class="row">����� ������: <span>�� � �� � 9:00 �� 18:00</span></div>
					<div class="row"><a href="#" style="text-decoration: underline;">����� �������</a></div>
				</div>
				<div class="contacts-blocks contacts-logo">
					<a href="#"><img src="<?=SITE_TEMPLATE_PATH?>/img/logo-white.png" alt="logo"></a>
				</div>
				<div class="contacts-blocks contacts-adress">
					<div class="contact-block">
					<div class="row">E-mail: <a href="mailto:8829945@mail.ru">8829945@mail.ru</a></div>
					<div class="row">�����:  <span>�. ������, �. ��������,</span></div>
					<div class="row"><span>��������������� �����, � 11</span></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!-- footer	 -->
	<section id="footer">
		<div class="container-fluide">
			<div class="row fix insert">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding footer-left">
					<p>
						&copy; 2016. ��� ����� ��������
					</p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding footer-right">
					<div class="social-block">
						<a href="#"><i class="fa fa-lg fa-vk" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-lg fa-twitter" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-lg fa-facebook" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-lg fa-instagram" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-lg fa-google" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		</div>	
	</section>


<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => SITE_TEMPLATE_PATH."/include/modal.php"
    )
);?>


<script type="text/javascript">
  $(document).ready(function() {
    if ($(window).width() < 480) {
    	 var f1 = $(".footer-left");
    	 var f2 = $(".footer-right");
	 	 var cont1 = $(".contacts-logo");
		 var cont2 = $(".contacts-adress");
		 var cont3 = $(".contacts-work");
    	 $(".insert").empty().append(f2, f1);
	     $(".contacts").empty().append(cont3, cont2, cont1);

	}
  });
</script>
	
	<script>
  	$(document).ready(function () {
  		$('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;
  
    $this.addClass('select-hidden'); 
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());
  
    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);
  
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }
  
    var $listItems = $list.children('li');
  
    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });
  
    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });
  
    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});
  	});
  </script>




</body>
</html>